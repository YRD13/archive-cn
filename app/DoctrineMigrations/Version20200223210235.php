<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200223210235 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE TABLE subscription (id SERIAL NOT NULL, user_id INT NOT NULL, created_at TIMESTAMP(0) WITH TIME ZONE NOT NULL, end_contract_at TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL, amount_paid DOUBLE PRECISION NOT NULL, status VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_A3C664D3A76ED395 ON subscription (user_id)');
        $this->addSql('CREATE TABLE themes (id SERIAL NOT NULL, "order" INT NOT NULL, type VARCHAR(255) NOT NULL, url VARCHAR(255) DEFAULT NULL, title VARCHAR(255) NOT NULL, subtitle VARCHAR(255) DEFAULT NULL, category VARCHAR(255) NOT NULL, souscategory VARCHAR(255) DEFAULT NULL, picture VARCHAR(255) DEFAULT NULL, header TEXT DEFAULT NULL, visible BOOLEAN DEFAULT NULL, expiration_date TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE statistics (id SERIAL NOT NULL, user_id INT DEFAULT NULL, action_name VARCHAR(255) NOT NULL, triggered_at TIMESTAMP(0) WITH TIME ZONE NOT NULL, content_id VARCHAR(255) DEFAULT NULL, content_title VARCHAR(255) DEFAULT NULL, content_type VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_E2D38B22A76ED395 ON statistics (user_id)');
        $this->addSql('CREATE TABLE users (id SERIAL NOT NULL, member_num VARCHAR(255) DEFAULT NULL, firstname VARCHAR(255) NOT NULL, lastname VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, phone VARCHAR(255) DEFAULT NULL, password VARCHAR(255) NOT NULL, created_at TIMESTAMP(0) WITH TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITH TIME ZONE NOT NULL, deleted_at TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL, forgot_pwd_code VARCHAR(255) DEFAULT NULL, status VARCHAR(255) NOT NULL, token VARCHAR(255) DEFAULT NULL, role VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_1483A5E9E7927C74 ON users (email)');
        $this->addSql('CREATE TABLE sousthemes (id SERIAL NOT NULL, theme_id INT DEFAULT NULL, title VARCHAR(255) NOT NULL, picture VARCHAR(255) DEFAULT NULL, content TEXT NOT NULL, picture_only BOOLEAN DEFAULT \'false\' NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_E09EDEA759027487 ON sousthemes (theme_id)');
        $this->addSql('CREATE TABLE appinfo (id SERIAL NOT NULL, general_condition TEXT NOT NULL, subscription_price DOUBLE PRECISION DEFAULT NULL, perso_message_call_conseiller TEXT NOT NULL, perso_conseiller_phone VARCHAR(255) NOT NULL, perso_conseiller_mail_direct VARCHAR(255) NOT NULL, perso_website_url VARCHAR(255) NOT NULL, tr_message_call_conseiller TEXT NOT NULL, tr_conseiller_phone VARCHAR(255) NOT NULL, tr_conseiller_mail_callback VARCHAR(255) NOT NULL, tr_conseiller_mail_direct VARCHAR(255) NOT NULL, tr_website_accueil VARCHAR(255) NOT NULL, tr_website_sante VARCHAR(255) NOT NULL, tr_website_prevoyance VARCHAR(255) NOT NULL, tr_website_teleassistance VARCHAR(255) NOT NULL, promo_code VARCHAR(255) DEFAULT NULL, perso_conseiller_mail_callback VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE quizz (id SERIAL NOT NULL, user_id INT DEFAULT NULL, created_at TIMESTAMP(0) WITH TIME ZONE NOT NULL, custom_id VARCHAR(255) DEFAULT NULL, quizz001 VARCHAR(255) DEFAULT NULL, quizz004 VARCHAR(255) DEFAULT NULL, quizz006 VARCHAR(255) DEFAULT NULL, quizz008 VARCHAR(255) DEFAULT NULL, quizz007 VARCHAR(255) DEFAULT NULL, quizz009 VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_7C77973DA76ED395 ON quizz (user_id)');
        $this->addSql('ALTER TABLE subscription ADD CONSTRAINT FK_A3C664D3A76ED395 FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE statistics ADD CONSTRAINT FK_E2D38B22A76ED395 FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE sousthemes ADD CONSTRAINT FK_E09EDEA759027487 FOREIGN KEY (theme_id) REFERENCES themes (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE quizz ADD CONSTRAINT FK_7C77973DA76ED395 FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE sousthemes DROP CONSTRAINT FK_E09EDEA759027487');
        $this->addSql('ALTER TABLE subscription DROP CONSTRAINT FK_A3C664D3A76ED395');
        $this->addSql('ALTER TABLE statistics DROP CONSTRAINT FK_E2D38B22A76ED395');
        $this->addSql('ALTER TABLE quizz DROP CONSTRAINT FK_7C77973DA76ED395');
        $this->addSql('DROP TABLE subscription');
        $this->addSql('DROP TABLE themes');
        $this->addSql('DROP TABLE statistics');
        $this->addSql('DROP TABLE users');
        $this->addSql('DROP TABLE sousthemes');
        $this->addSql('DROP TABLE appinfo');
        $this->addSql('DROP TABLE quizz');
    }
}
