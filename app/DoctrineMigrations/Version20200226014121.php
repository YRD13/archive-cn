<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200226014121 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE TABLE souscategories (id SERIAL NOT NULL, categorie_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, code_name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_6C660C3ABCF5E72D ON souscategories (categorie_id)');
        $this->addSql('CREATE TABLE categories (id SERIAL NOT NULL, name VARCHAR(255) NOT NULL, code_name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE souscategories ADD CONSTRAINT FK_6C660C3ABCF5E72D FOREIGN KEY (categorie_id) REFERENCES categories (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE themes ADD souscategorie_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE themes ADD CONSTRAINT FK_154232DEA27126E0 FOREIGN KEY (souscategorie_id) REFERENCES souscategories (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_154232DEA27126E0 ON themes (souscategorie_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE themes DROP CONSTRAINT FK_154232DEA27126E0');
        $this->addSql('ALTER TABLE souscategories DROP CONSTRAINT FK_6C660C3ABCF5E72D');
        $this->addSql('DROP TABLE souscategories');
        $this->addSql('DROP TABLE categories');
        $this->addSql('DROP INDEX IDX_154232DEA27126E0');
        $this->addSql('ALTER TABLE themes DROP souscategorie_id');
    }
}
