#!/bin/bash
php bin/console c:c --env=prod
#chmod -R 777 var/ web/img/ web/ressources/ web/images/ web/img/theme_assets/

find . -type f -exec chmod 644 {} \;
find . -type d -exec chmod 755 {} \;
chown -R root: .
chown -R www-data: var web/img web/ressources web/images
find var -type f -exec chmod 600 {} \;
chmod u+x ./deploy.sh
touch /tmp/integrity_check_witness