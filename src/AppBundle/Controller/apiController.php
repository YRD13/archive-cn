<?php
/**
 * Created by PhpStorm.
 * User: yr
 * Date: 17/11/2017
 * Time: 01:54
 */

namespace AppBundle\Controller;


use AppBundle\Entity\Appinfo;
use AppBundle\Entity\Ratings;
use AppBundle\Entity\Sousthemes;
use AppBundle\Entity\Statistics;
use AppBundle\Entity\Subscription;
use AppBundle\Entity\Themes;
use AppBundle\Entity\Users;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class apiController extends Controller
{

    /**
     * @param Request $request
     * @return Response
     * @Route("/api/track", options={"expose"=true}, name="ws_track")
     * @Method("POST")
     */
    public function ws_track(Request $request){

        if ($request->isXMLHttpRequest()) {

            $session = $request->getSession();
            /**
             * @var $userInfo Users
             */
            $userInfo = $session->get('userInfo')['profile'];
            $data['action_name'] = $request->get('action_name');
            $data['content_id'] = $request->get('content_id');
            $data['content_title'] = $request->get('content_title');
            $data['content_type'] = $request->get('content_type');

            if(!$session->has('userInfo') ||$userInfo->getRole() != 'CLIENT'){
                $error = ['error' => 'Une erreur est survenue', 'msg' => ''];
                return new JsonResponse($error);
            }
            if(!isset($data['action_name'])
                || ($data['action_name'] != 'MAIL_CS' && $data['action_name'] != 'RAPPEL_CS' && $data['action_name'] != 'APPEL_CS'
                    && $data['action_name'] != 'INSCRIPTION' && $data['action_name'] != 'CONSULT_E_LEARNING'
                    && $data['action_name'] != 'CONSULT_CONTENT' && $data['action_name'] != 'CONSULT_VIDEO' && $data['action_name'] != 'CONSULT_QUIZZ')){
                $error = ['error' => 'Information manquante', 'msg' => 'Veuillez remplir tous les champs'];
                return new JsonResponse($error);
            }


            $em = $this->getDoctrine()->getManager();
            $user = $em->getRepository('AppBundle:Users')->find($userInfo->getId());
            if(!$user){
                $error = ['error' => 'Une erreur est survenue', 'msg' => ''];
                return new JsonResponse($error);
            }

            /*
            *  SAVE STAT HERE
            *
            */

            /**
             * @Var Statistics $stat
             */
            $stat = new Statistics();
            $stat->setActionName($data['action_name']);
            $stat->setUser($user);
            if(isset($data['content_id']) && $data['content_id'] != '') $stat->setContentId($data['content_id']);
            if(isset($data['content_title']) && $data['content_title'] != '') $stat->setContentTitle($data['content_title']);
            if(isset($data['content_type']) && $data['content_type'] != '') $stat->setContentType($data['content_type']);
            $em->persist($stat);
            $em->flush();

            $success = ["success" => true];
            return new JsonResponse($success);

        }else{
            return new Response('BAD REQUEST!', 400);
        }
    }


    /**
     * @param Request $request
     * @return Response
     * @Route("/api/rating", options={"expose"=true}, name="ws_rating")
     * @Method("POST")
     */
    public function ws_rating(Request $request){

        if ($request->isXMLHttpRequest()) {

            $session = $request->getSession();
            /**
             * @var $userInfo Users
             */
            $userInfo = $session->get('userInfo')['profile'];
            $data['rate'] = $request->get('rate');
            $data['soustheme_id'] = $request->get('soustheme_id');

            if(!$session->has('userInfo') || $userInfo->getRole() != 'CLIENT'){
                $error = ['error' => 'Une erreur est survenue', 'msg' => ''];
                return new JsonResponse($error);
            }

            $em = $this->getDoctrine()->getManager();
            $user = $em->getRepository('AppBundle:Users')->find($userInfo->getId());
            if(!$user){
                $error = ['error' => 'Une erreur est survenue', 'msg' => ''];
                return new JsonResponse($error);
            }

            /** @var Sousthemes $soustheme */
            $soustheme = $em->getRepository(Sousthemes::class)->find($data['soustheme_id']);
            if(!$soustheme){
                $error = ['error' => 'Une erreur est survenue', 'msg' => ''];
                return new JsonResponse($error);
            }

            $rating = $em->getRepository(Ratings::class)->findOneBy([
                'soustheme' => $soustheme,
                'user' => $user
            ]);

            if(!$rating){
                /**
                 * @Var Ratings $rating
                 */
                $rating = new Ratings();
                $rating->setRate($data['rate']);
                $rating->setUser($user);
                $rating->setSoustheme($soustheme);
                $em->persist($rating);
            }else{
                $rating->setRate($data['rate']);
                $rating->setUser($user);
                $rating->setSoustheme($soustheme);
            }
            $em->flush();

            $success = ["success" => true];
            return new JsonResponse($success);

        }else{
            return new Response('BAD REQUEST!', 400);
        }
    }


    /**
     * @param Request $request
     * @return Response
     * @Route("/api/quizz", options={"expose"=true}, name="ws_get_quizz")
     * @Method("POST")
     */
    public function ws_get_quizz(Request $request){

        if ($request->isXMLHttpRequest()) {

            $session = $request->getSession();
            /**
             * @var $userInfo Users
             */
            $userInfo = $session->get('userInfo')['profile'];

            $type = $request->get('type');

            if(!$type || !$session->has('userInfo') || $userInfo->getRole() != 'CLIENT'){
                $error = ['error' => 'Une erreur est survenue', 'msg' => ''];
                return new JsonResponse($error);
            }

            $em = $this->getDoctrine()->getManager();
            $user = $em->getRepository('AppBundle:Users')->find($userInfo->getId());
            if(!$user){
                $error = ['error' => 'Une erreur est survenue', 'msg' => ''];
                return new JsonResponse($error);
            }

            $quizz = $em->getRepository('AppBundle:Quizz')->findOneBy(['user' => $user, 'type' => $type]);

            if(!$quizz){
                $quizz = $em->getRepository('AppBundle:Quizz')->findOneBy(['user' => null, 'type' => $type]);
                $quizz->setUser($user);

                $em->persist($quizz);
                $em->flush();
            }

            $success = ["success" => true, "url" => $quizz->getUrl()];
            return new JsonResponse($success);

        }else{
            return new Response('BAD REQUEST!', 400);
        }
    }

    /**
     * @param Request $request
     * @param \Swift_Mailer $mailer
     * @return Response
     * @Route("/api/inscription", options={"expose"=true}, name="ws_inscription")
     * @Method("POST")
     * @throws \Exception
     */
    public function ws_inscription(Request $request, \Swift_Mailer $mailer){

        if ($request->isXMLHttpRequest()) {


            $data['firstname'] = trim($request->get('firstname'));
            $data['lastname'] = trim($request->get('lastname'));
            $data['email'] = trim($request->get('email'));
            $data['member_num'] = trim($request->get('member_num'));
            $data['phone'] = trim($request->get('phone'));
            $data['password'] = trim($request->get('password'));
            $data['password2'] = trim($request->get('password2'));

            $inputRequired= ["firstname","lastname","email","password","password2"];
            $arrayErrorNameInput  =[];
            /*Check if Empty*/
            foreach ($inputRequired as $nameInput){
                if(trim($request->get($nameInput) == '' || empty($request->get($nameInput))) ){
                    array_push($arrayErrorNameInput,$nameInput);
                }
            }

            if(!empty($arrayErrorNameInput)){
                $error = ['error' => 'Information manquante', 'msg' => 'Veuillez remplir tous les champs obligatoires','input_name' => $arrayErrorNameInput];
                return new JsonResponse($error);
            }

            if($data['password'] != $data['password2']){
                $error = ['error' => 'Information Erronée', 'msg' => 'Les mots de passe ne sont pas similaires', 'input_name' => 'password'];
                return new JsonResponse($error);
            }

            if(!filter_var($data['email'], FILTER_VALIDATE_EMAIL)){

                $error = ['error' => 'Information Erronée', 'msg' => 'Veuillez saisir une adresse email valide', 'input_name' => 'email'];
                return new JsonResponse($error);
            }

            if($data['phone'] != '' && preg_match('/^(?:(?:\+|00)33|0)\s*[1-9](?:[\s.-]*\d{2}){4}$/', $data['phone']) != 1){
                $error = ['error' => 'Information Erronée', 'msg' => 'Veuillez saisir un numéro de téléphone valide', 'input_name' => 'phone'];
                return new JsonResponse($error);
            }

            if(preg_match('/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/', $data['password']) != 1){

                $error = ['error' => 'Information Erronée', 'msg' => 'Veuillez saisir un mot de passe avec au moins 8 caractères et 1 chiffre sans caractères spéciaux ni ponctuation', 'input_name' => 'password'];
                return new JsonResponse($error);
            }


            $em = $this->getDoctrine()->getManager();
            $checkUser = $em->getRepository('AppBundle:Users')->findOneBy(['email' => $data['email']]);

            $appInfo = $em->getRepository('AppBundle:Appinfo')
                ->find(1);

            if($checkUser){
                $error = ['error' => 'Email déjà enregistré', 'msg' => 'Veuillez saisir une autre adresse email'];
                return new JsonResponse($error);
            }

            /*
            *  SAVE USER HERE
            *
            */


            $user = new Users();
            $user->setFirstname($data['firstname']);
            $user->setLastname($data['lastname']);
            $user->setEmail($data['email']);
            $user->setMemberNum($data['member_num']);
            $user->setPassword(md5($data['password']));
            $user->setPhone($data['phone']);
            $user->setToken(bin2hex(random_bytes(20)));
            $user->setRole('CLIENT');

            $em->persist($user);

            $subscription = new Subscription();
            $subscription->setStatus('PENDING');
            $subscription->setAmountPaid($appInfo->getSubscriptionPrice());
            $subscription->setUser($user);

            $em->persist($subscription);

            $quizz = $em->getRepository('AppBundle:Quizz')->findOneBy(['user' => null]);
            $quizz->setUser($user);

            $em->persist($quizz);

            $stat = new Statistics();
            $stat->setActionName('INSCRIPTION - PENDING');
            $stat->setUser($user);
            $em->persist($stat);

            $em->flush();

            /*
             *
             *  ENVOIE EMAIL
             *
             */

            $message = (new \Swift_Message('Confirmation d\'inscription à votre espace Solidarité Coups Durs'))
                ->setFrom(array($this->container->getParameter('mailer_alias') => 'Macif Solidarité Coups Durs'))
                ->setTo($data['email'])
                ->setBody(
                    $this->renderView('emails/confirmation.html.twig', [
                        'firstname' => $data['firstname'],
                        'lastname' => $data['lastname'],
                        'hash_id' => base64_encode(base64_encode('STKSOLUTIONS_'.$user->getId())),
                    ]),
                    'text/html'
                );

            if ($recipients = $mailer->send($message, $failures))
            {
                $success = ["success" => true];
                return new JsonResponse($success);

            } else {
                $error = ['error' => 'Confirmation d\'email', 'msg' => 'Une erreur est survenue.'];
                return new JsonResponse($error);
            }
        }else{
            return new Response('BAD REQUEST!', 400);
        }
    }

    /**
     * @param Request $request
     * @return Response
     * @Route("/api/profile", options={"expose"=true}, name="ws_update_profile")
     * @Method("POST")
     * @throws \Exception
     */
    public function ws_update_profile(Request $request){

        if ($request->isXMLHttpRequest()) {

            $session = $request->getSession();
            if(!$session->has('userInfo') || $session->get('userInfo')['profile']->getRole() != 'CLIENT'){
                $error = ['error' => 'Une erreur est survenue', 'msg' => ''];
                return new JsonResponse($error);
            }
            /**
             * @var $userInfo Users
             */
            $userInfo = $session->get('userInfo');

            $data['firstname'] = trim($request->get('firstname'));
            $data['lastname'] = trim($request->get('lastname'));
            $data['email'] = trim($request->get('email'));
            $data['member_num'] = trim($request->get('member_num'));
            $data['phone'] = trim($request->get('phone'));
            $data['password'] = trim($request->get('password'));
            $data['password2'] = trim($request->get('password2'));

            $inputRequired = ["firstname","lastname","email"];
            $arrayErrorNameInput = [];
            /*Check if Empty*/
            foreach ($inputRequired as $nameInput){
                if(trim($request->get($nameInput) == '' || empty($request->get($nameInput))) ){
                    array_push($arrayErrorNameInput,$nameInput);
                }
            }

            if(!empty($arrayErrorNameInput)){
                $error = ['error' => 'Information manquante', 'msg' => 'Veuillez remplir tous les champs obligatoires','input_name' => $arrayErrorNameInput];
                return new JsonResponse($error);
            }

            if($data['password'] != $data['password2']){
                $error = ['error' => 'Information Erronée', 'msg' => 'Les mots de passe ne sont pas similaires', 'input_name' => 'password'];
                return new JsonResponse($error);
            }

            if(!filter_var($data['email'], FILTER_VALIDATE_EMAIL)){

                $error = ['error' => 'Information Erronée', 'msg' => 'Veuillez saisir une adresse email valide', 'input_name' => 'email'];
                return new JsonResponse($error);
            }

            $arrayPhone = explode(' ', $data['phone']);

            foreach($arrayPhone as $phone){
                if($phone == "" || preg_match('/^(?:(?:\+|00)33|0)\s*[1-9](?:[\s.-]*\d{2}){4}$/', $phone) != 1){
                    $error = ['error' => 'Une erreur est survenue', 'msg' => 'Veuillez saisir un numéro de téléphone valide'];
                    return new JsonResponse($error);
                }
            }

            if(preg_match('/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/', $data['password']) != 1 && !empty($data['password'])){

                $error = ['error' => 'Information Erronée', 'msg' => 'Veuillez saisir un mot de passe avec au moins 8 caractères et 1 chiffre sans caractères spéciaux ni ponctuation', 'input_name' => 'password'];
                return new JsonResponse($error);
            }


            $em = $this->getDoctrine()->getManager();
            $user = $em->getRepository(Users::class)->find($userInfo['profile']->getId());


            if(!$user){
                $error = ['error' => 'Utilisateur inconnu', 'msg' => ''];
                return new JsonResponse($error);
            }

            /*
            *  UPDATE USER HERE
            *
            */

            $user->setFirstname($data['firstname']);
            $user->setLastname($data['lastname']);
            $user->setEmail($data['email']);
            $user->setMemberNum($data['member_num']);
            $user->setPhone($data['phone']);

            if(!empty($data['password']))
                $user->setPassword(md5($data['password']));

            $em->flush();

            $userInfo['profile'] = $user;
            $session->set('userInfo', $userInfo);


            $success = ["success" => true];
            return new JsonResponse($success);

        }else{
            return new Response('BAD REQUEST!', 400);
        }
    }


    /**
     * @param Request $request
     * @param \Swift_Mailer $mailer
     * @return Response
     * @Route("/api/password", options={"expose"=true}, name="ws_password")
     * @Method("POST")
     * @throws \Exception
     */
    public function ws_password(Request $request, \Swift_Mailer $mailer){

        if ($request->isXMLHttpRequest()) {


            $data['email'] = $request->get('email');


            if(!isset($data['email']) || $data['email'] == ''){
                $error = ['error' => 'Information manquante', 'msg' => 'Veuillez remplir tous les champs'];
                return new JsonResponse($error);
            }

            if(!filter_var($data['email'], FILTER_VALIDATE_EMAIL)){

                $error = ['error' => 'Information Erronée', 'msg' => 'Veuillez saisir une adresse email valide'];
                return new JsonResponse($error);
            }


            /*
            *  SAVE USER HERE
            *
            */

            $em = $this->getDoctrine()->getManager();

            $user = $em->getRepository('AppBundle:Users')
                ->findOneBy([
                    'email' => $data['email']
                ]);

            if(!$user){
                $error = ['error' => 'Email inconnue', 'msg' => 'Veuillez vérifier votre saisie.'];
                return new JsonResponse($error);
            }

            $resetCode = bin2hex(random_bytes(20));
            $user->setForgotPwdCode($resetCode);
            $em->persist($user);
            $em->flush();


            /*
             *
             *  ENVOIE EMAIL
             *
             */

            $message = (new \Swift_Message('Réinitialisation de mot de passe'))
                ->setFrom(array($this->container->getParameter('mailer_alias') => 'Macif Solidarité Coups Durs'))
                ->setTo($data['email'])
                ->setBody(
                    $this->renderView('emails/password.html.twig', [
                        'hash_id' => $resetCode,
                    ]),
                    'text/html'
                );

            if ($recipients = $mailer->send($message, $failures))
            {
                $success = ["success" => true];
                return new JsonResponse($success);

            } else {
                $error = ['error' => 'Réinitialisation de mot de passe', 'msg' => 'Une erreur est survenue.'];
                return new JsonResponse($error);
            }
        }else{
            return new Response('BAD REQUEST!', 400);
        }
    }

    /**
     * @param Request $request
     * @return Response
     * @Route("/api/updatepassword/{hashId}", options={"expose"=true}, name="ws_resetpassword")
     * @Method("POST")
     */
    public function ws_updatepassword(Request $request, $hashId){

        if ($request->isXMLHttpRequest()) {

            $data['password'] = $request->get('password');
            $data['password2'] = $request->get('password2');

            if(!isset($data['password']) || $data['password'] == ''
                || !isset($data['password2']) || $data['password2'] == ''){
                $error = ['error' => 'Information Manquante', 'msg' => 'Veuillez remplir tous les champs'];
                return new JsonResponse($error);
            }

            if($data['password'] != $data['password2']){
                $error = ['error' => 'Information Erronée', 'msg' => 'Les mots de passe ne sont pas identiques'];
                return new JsonResponse($error);
            }

            if(preg_match('/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/', $data['password']) != 1){

                $error = ['error' => 'Information Erronée', 'msg' => 'Veuillez saisir un mot de passe avec au moins 8 caractères et 1 chiffre'];
                return new JsonResponse($error);
            }

            $em = $this->getDoctrine()->getManager();
            $user = $em->getRepository('AppBundle\Entity\Users')
                ->findOneBy(['forgotPwdCode' => $hashId]);

            if($user != null) {
                $user->setPassword(md5($data['password']));
                $user->setForgotPwdCode(null);

                $em->persist($user);
                $em->flush();

                $success = ["success" => true];
                return new JsonResponse($success);
            }
            return $this->redirectToRoute('auth');
        }else{
            return new Response('BAD REQUEST!', 400);
        }
    }

    /**
     * @param Request $request
     * @param \Swift_Mailer $mailer
     * @return Response
     * @Route("/api/callbackperso", options={"expose"=true}, name="ws_callbackperso")
     * @Method("POST")
     */
    public function ws_callbackperso(Request $request, \Swift_Mailer $mailer){

        if ($request->isXMLHttpRequest()) {
            $session = $request->getSession();
            if(!$session->has('userInfo') || $session->get('userInfo')['profile']->getRole() != 'CLIENT'){
                $error = ['error' => 'Une erreur est survenue', 'msg' => ''];
                return new JsonResponse($error);
            }

            $data['firstname'] = $request->get('firstname');
            $data['lastname'] = $request->get('lastname');
            $data['phone'] = $request->get('phone');
            $data['gender'] = $request->get('gender');
            $data['email'] = $request->get('email');


            if(!filter_var($data['email'], FILTER_VALIDATE_EMAIL)){

                $error = ['error' => 'Une erreur est survenue', 'msg' => ''];
                return new JsonResponse($error);
            }

            $arrayPhone = explode(' ', $data['phone']);

            foreach($arrayPhone as $phone){
                if($phone == "" || preg_match('/^(?:(?:\+|00)33|0)\s*[1-9](?:[\s.-]*\d{2}){4}$/', $phone) != 1){
                    $error = ['error' => 'Une erreur est survenue', 'msg' => 'Veuillez saisir un numéro de téléphone valide'];
                    return new JsonResponse($error);
                }
            }



            /*
            *  GET APP INFO
            *
            */

            $em = $this->getDoctrine()->getManager();

            $appInfo = $em->getRepository('AppBundle:Appinfo')
                ->find(1);



            /*
             *
             *  ENVOIE EMAIL
             *
             */

            $message = (new \Swift_Message('[Demande de rappel] - Conseiller personnel'))
                ->setFrom($this->container->getParameter('mailer_alias'))
                ->setTo($appInfo->getPersoConseillerMailDirect())
                ->setBody(
                    $this->renderView('emails/callback.html.twig', [
                        'firstname' => $data['firstname'],
                        'lastname' => $data['lastname'],
                        'gender' => $data['gender'],
                        'phone' => $data['phone'],
                        'email' => $data['email']
                    ]),
                    'text/html'
                );

            if ($recipients = $mailer->send($message, $failures))
            {
                $success = ["success" => true];
                return new JsonResponse($success);

            } else {
                $error = ['error' => 'Une erreur est survenue', 'msg' => ''];
                return new JsonResponse($error);
            }
        }else{
            return new Response('BAD REQUEST!', 400);
        }
    }

    /**
     * @param Request $request
     * @param \Swift_Mailer $mailer
     * @return Response
     * @Route("/api/callbacktr", options={"expose"=true}, name="ws_callbacktr")
     * @Method("POST")
     */
    public function ws_callbacktr(Request $request, \Swift_Mailer $mailer){

        if ($request->isXMLHttpRequest()) {
            $session = $request->getSession();
            if(!$session->has('userInfo') || $session->get('userInfo')['profile']->getRole() != 'CLIENT'){
                $error = ['error' => 'Une erreur est survenue', 'msg' => ''];
                return new JsonResponse($error);
            }

            $data['firstname'] = $request->get('firstname');
            $data['lastname'] = $request->get('lastname');
            $data['phone'] = $request->get('phone');
            $data['email'] = $request->get('email');
            $data['birthDate'] = $request->get('birthDate');
            $data['message'] = $request->get('message');
            $data['contactDayMon'] = $request->get('contactDayMon');
            $data['contactDayTue'] = $request->get('contactDayTue');
            $data['contactDayWed'] = $request->get('contactDayWed');
            $data['contactDayThu'] = $request->get('contactDayThu');
            $data['contactDayFri'] = $request->get('contactDayFri');
            $data['matin'] = $request->get('matin');
            $data['apresMidi'] = $request->get('apresMidi');


            if(!filter_var($data['email'], FILTER_VALIDATE_EMAIL)){
                $error = ['error' => 'Une erreur est survenue', 'msg' => ''];
                return new JsonResponse($error);
            }

            $arrayPhone = explode(' ', $data['phone']);

            foreach($arrayPhone as $phone){
                if($phone == "" || preg_match('/^(?:(?:\+|00)33|0)\s*[1-9](?:[\s.-]*\d{2}){4}$/', $phone) != 1){
                    $error = ['error' => 'Une erreur est survenue', 'msg' => 'Veuillez saisir un numéro de téléphone valide'];
                    return new JsonResponse($error);
                }
            }

            if($data['message'] == ""){
                $error = ['error' => 'Une erreur est survenue', 'msg' => 'Veuillez renseigner un message'];
                return new JsonResponse($error);
            }



            /*
            *  GET APP INFO
            *
            */

            $em = $this->getDoctrine()->getManager();

            /**
             * @var Appinfo $appInfo
             */
            $appInfo = $em->getRepository('AppBundle:Appinfo')
                ->find(1);

            /*
             *
             *  ENVOIE EMAIL
             *
             */

            $contactDays = [];
            foreach($data as $element => $value){
                if(strpos($element, 'contactDay') !== false && $value == true){
                    switch ($element){
                        case 'contactDayMon':
                            $contactDays[] = 'Lundi';
                            break;
                        case 'contactDayTue':
                            $contactDays[] = 'Mardi';
                            break;
                        case 'contactDayWed':
                            $contactDays[] = 'Mercredi';
                            break;
                        case 'contactDayThu':
                            $contactDays[] = 'Jeudi';
                            break;
                        case 'contactDayFri':
                            $contactDays[] = 'Vendredi';
                            break;
                    }
                }
            }

            if(empty($contactDays)){
                $error = ['error' => 'Une erreur est survenue', 'msg' => 'Veuillez renseigner un jour de rappel'];
                return new JsonResponse($error);
            }


            if($data['matin'] == "false" && $data['apresMidi'] == "false" ){
                $error = ['error' => 'Une erreur est survenue', 'msg' => 'Veuillez renseigner le moment de la journée de rappel'];
                return new JsonResponse($error);
            }

            $contactDays = implode(' | ', $contactDays);

            $objet = 'Demande de rappel – WebApp MACIF ' . $data['lastname'] . ' '. $data['firstname'] ;
            $message = (new \Swift_Message($objet))
                ->setFrom($this->container->getParameter('mailer_alias'))
                ->setTo($appInfo->getTrConseillerMailCallback())
                ->setBody(
                    $this->renderView('emails/callback.html.twig', [
                        'firstname' => $data['firstname'],
                        'lastname' => $data['lastname'],
                        'phone' => $data['phone'],
                        'email' => $data['email'],
                        'birthDate' => $data['birthDate'],
                        'message' => $data['message'],
                        'contactDays' => $contactDays,
                        'matin' => $data['matin'],
                        'apresMidi' => $data['apresMidi']
                    ]),
                    'text/html'
                );
            if ($recipients = $mailer->send($message, $failures))
            {
                $success = ["success" => true];
                return new JsonResponse($success);

            } else {
                $error = ['error' => 'Une erreur est survenue', 'msg' => ''];
                return new JsonResponse($error);
            }
        }else{
            return new Response('BAD REQUEST!', 400);
        }
    }



    /**
     * @param Request $request
     * @param \Swift_Mailer $mailer
     * @return Response
     * @Route("/api/contacttr", options={"expose"=true}, name="ws_contacttr")
     * @Method("POST")
     */
    public function ws_contacttr(Request $request, \Swift_Mailer $mailer){

        if ($request->isXMLHttpRequest()) {
            $session = $request->getSession();
            if(!$session->has('userInfo') || $session->get('userInfo')['profile']->getRole() != 'CLIENT'){
                $error = ['error' => 'Une erreur est survenue', 'msg' => ''];
                return new JsonResponse($error);
            }

            $data['firstname'] = $request->get('firstname');
            $data['lastname'] = $request->get('lastname');
            $data['email'] = $request->get('email');
            $data['message'] = $request->get('message');
            $data['birthDate'] = $request->get('birthDate');


            if(!filter_var($data['email'], FILTER_VALIDATE_EMAIL)){
                $error = ['error' => 'Une erreur est survenue', 'msg' => 'Veuillez saisir une adresse e-mail valide'];
                return new JsonResponse($error);
            }

            if($data['message'] == ""){
                $error = ['error' => 'Une erreur est survenue', 'msg' => 'Veuillez renseigner un message'];
                return new JsonResponse($error);
            }

            /*
            *  GET APP INFO
            *
            */

            $em = $this->getDoctrine()->getManager();

            /* @var Appinfo $appInfo */
            $appInfo = $em->getRepository('AppBundle:Appinfo')
                ->find(1);

            /*
             *
             *  ENVOIE EMAIL
             *
             */
            $objet = 'Echange Mail – WebApp MACIF ' . $data['lastname'] . ' '. $data['firstname'] ;

            $message = (new \Swift_Message($objet))
                ->setFrom($this->container->getParameter('mailer_alias'))
                ->setTo($appInfo->getTrConseillerMailDirect())
                ->setReplyTo($data['email'])
                ->setBody(
                    $this->renderView('emails/contact.html.twig', [
                        'firstname' => $data['firstname'],
                        'lastname' => $data['lastname'],
                        'email' => $data['email'],
                        'birthDate' => $data['birthDate'],
                        'message' => $data['message']
                    ]),
                    'text/html'
                );
            if ($recipients = $mailer->send($message, $failures))
            {
                $success = ["success" => true];
                return new JsonResponse($success);

            } else {
                $error = ['error' => 'Une erreur est survenue', 'msg' => ''];
                return new JsonResponse($error);
            }
        }else{
            return new Response('BAD REQUEST!', 400);
        }
    }


    /**
     * @param Request $request
     * @return Response
     * @Route("/api/getClients", options={"expose"=true}, name="getclients")
     * @Method("POST")
     */
    public function getClients(Request $request){

        if ($request->isXMLHttpRequest()) {
            $session = $request->getSession();
            if(!$session->has('userInfo') || $session->get('userInfo')['profile']->getRole() != 'ADMINISTRATOR'){
                $error = ['error' => 'Une erreur est survenue', 'msg' => ''];
                return new JsonResponse($error);
            }
            $em = $this->getDoctrine()->getManager();

            $clientList = $em->getRepository('AppBundle:Users')
                ->getClients();

            $content = [];
            $content['template_client'] = $this->render('ajax/clientlist.html.twig', ['clientList' => $clientList])->getContent();

            return new JsonResponse($content);
        }
        return new Response('BAD REQUEST!', 400);
    }

    /**
     * @param Request $request
     * @return Response
     * @Route("/api/getThemes", options={"expose"=true}, name="getthemes")
     * @Method("POST")
     */
    public function getThemes(Request $request){

        if ($request->isXMLHttpRequest()) {
            $session = $request->getSession();
            if(!$session->has('userInfo') || $session->get('userInfo')['profile']->getRole() != 'ADMINISTRATOR'){
                $error = ['error' => 'Une erreur est survenue', 'msg' => ''];
                return new JsonResponse($error);
            }
            $em = $this->getDoctrine()->getManager();

            $themeList = $em->getRepository('AppBundle:Themes')->findBy([], ['category' => 'ASC', 'souscategory' => 'ASC', 'order' => 'ASC']);

            $content = [];
            $content['template_theme'] = $this->render('ajax/themeslist.html.twig', ['themeList' => $themeList])->getContent();
            $content['template_theme_option'] = $this->render('ajax/themesoptions.html.twig', ['themeList' => $themeList])->getContent();

            return new JsonResponse($content);
        }
        return new Response('BAD REQUEST!', 400);
    }


    /**
     * @param Request $request
     * @return Response
     * @Route("/api/getSousThemes", options={"expose"=true}, name="getsousthemes")
     * @Method("POST")
     */
    public function getSousThemes(Request $request){

        if ($request->isXMLHttpRequest()) {
            $session = $request->getSession();
            if(!$session->has('userInfo') || $session->get('userInfo')['profile']->getRole() != 'ADMINISTRATOR'){
                $error = ['error' => 'Une erreur est survenue', 'msg' => ''];
                return new JsonResponse($error);
            }
            $em = $this->getDoctrine()->getManager();

            $themeList = $em->getRepository('AppBundle:Themes')->findBy([], ['category' => 'ASC', 'souscategory' => 'ASC', 'order' => 'ASC']);

            $content = [];
            $content['template_article'] = $this->render('ajax/articleslist.html.twig', ['themeList' => $themeList])->getContent();

            return new JsonResponse($content);
        }
        return new Response('BAD REQUEST!', 400);
    }

    /**
     * @param Request $request
     * @return Response
     * @Route("/api/getThemeById", options={"expose"=true}, name="getthemebyid")
     * @Method("POST")
     */
    public function getThemeById(Request $request){

        if ($request->isXMLHttpRequest()) {
            $session = $request->getSession();
            if(!$session->has('userInfo') || $session->get('userInfo')['profile']->getRole() != 'ADMINISTRATOR'){
                $error = ['error' => 'Une erreur est survenue', 'msg' => ''];
                return new JsonResponse($error);
            }

            $data['id'] = $request->get('theme_id');

            if(!isset($data['id']) || $data['id'] == ''){
                $error = ['title' => 'INFORMATION MANQUANTE', 'error' => 'Veuillez renseigner toutes les informations nécessaires'];
                return new JsonResponse($error);
            }

            $em = $this->getDoctrine()->getManager();

            $theme = $em->getRepository('AppBundle:Themes')->find($data['id']);

            $dataToReturn = [
                'title' => $theme->getTitle(),
                'picture' => $theme->getPicture(),
                'url' => $theme->getUrl(),
                'category' => $theme->getCategory(),
                'souscategory' => $theme->getSouscategory(),
                'expiration_date' => ($theme->getExpirationDate() != '') ? $theme->getExpirationDate()->format('d/m/Y') : '',
                'type' => $theme->getType(),
                'header' => $theme->getHeader()
            ];

            return new JsonResponse($dataToReturn);
        }
        return new Response('BAD REQUEST!', 400);
    }

    /**
     * @param Request $request
     * @return Response
     * @Route("/api/getSousThemeById", options={"expose"=true}, name="getsousthemebyid")
     * @Method("POST")
     */
    public function getSousThemeById(Request $request){

        if ($request->isXMLHttpRequest()) {
            $session = $request->getSession();
            if(!$session->has('userInfo') || $session->get('userInfo')['profile']->getRole() != 'ADMINISTRATOR'){
                $error = ['error' => 'Une erreur est survenue', 'msg' => ''];
                return new JsonResponse($error);
            }

            $data['id'] = $request->get('soustheme_id');

            if(!isset($data['id']) || $data['id'] == ''){
                $error = ['title' => 'INFORMATION MANQUANTE', 'error' => 'Veuillez renseigner toutes les informations nécessaires'];
                return new JsonResponse($error);
            }

            $em = $this->getDoctrine()->getManager();

            $theme = $em->getRepository('AppBundle:Sousthemes')->find($data['id']);

            $dataToReturn = [
                'title' => $theme->getTitle(),
                'picture' => $theme->getPicture(),
                'content' => $theme->getContent(),
                'theme_id' => $theme->getTheme()->getId(),
                'picture_only' => $theme->getPictureOnly()
            ];

            return new JsonResponse($dataToReturn);
        }
        return new Response('BAD REQUEST!', 400);
    }

    /**
     * @param Request $request
     * @return Response
     * @Route("/api/toggleTheme", options={"expose"=true}, name="toggletheme")
     * @Method("POST")
     */
    public function toggleTheme(Request $request){

        if ($request->isXMLHttpRequest()) {
            $session = $request->getSession();
            if(!$session->has('userInfo') || $session->get('userInfo')['profile']->getRole() != 'ADMINISTRATOR'){
                $error = ['error' => 'Une erreur est survenue', 'msg' => ''];
                return new JsonResponse($error);
            }

            $data['id'] = $request->get('id');

            if(!isset($data['id']) || $data['id'] == ''){
                $error = ['title' => 'INFORMATION MANQUANTE', 'error' => 'Veuillez renseigner toutes les informations nécessaires'];
                return new JsonResponse($error);
            }

            $em = $this->getDoctrine()->getManager();

            $theme = $em->getRepository('AppBundle:Themes')
                ->find($data['id']);

            if (!$theme) {
                $response = ['title' => 'INFORMATION MANQUANTE', 'error' => 'Veuillez renseigner toutes les informations nécessaires'];
                return new JsonResponse($response);
            }

            if($theme->getVisible() == true){
                $theme->setVisible(false);
            }else{
                $theme->setVisible(true);
            }

            $em->flush();
            $success = ["success" => true];
            return new JsonResponse($success);
        }
        return new Response('BAD REQUEST!', 400);
    }

    /**
     * @param Request $request
     * @return Response
     * @Route("/api/deleteclient", options={"expose"=true}, name="deleteclient")
     * @Method("POST")
     */
    public function deleteClient(Request $request){

        if ($request->isXMLHttpRequest()) {
            $session = $request->getSession();
            if(!$session->has('userInfo') || $session->get('userInfo')['profile']->getRole() != 'ADMINISTRATOR'){
                $error = ['error' => 'Une erreur est survenue', 'msg' => ''];
                return new JsonResponse($error);
            }

            $data['clientId'] = $request->get('clientId');


            if(!isset($data['clientId']) || $data['clientId'] == ''){
                $error = ['title' => 'INFORMATION MANQUANTE', 'error' => 'Veuillez renseigner toutes les informations nécessaires'];
                return new JsonResponse($error);
            }


            $em = $this->getDoctrine()->getManager();

            $user = $em->getRepository('AppBundle:Users')
                ->find($data['clientId']);

            if (!$user) {
                $response = ['title' => 'INFORMATION MANQUANTE', 'error' => 'Veuillez renseigner toutes les informations nécessaires'];
                return new JsonResponse($response);
            }

            $em->remove($user);
            $em->flush();
            $success = ["success" => true];
            return new JsonResponse($success);
        }
        return new Response('BAD REQUEST!', 400);
    }

    /**
     * @param Request $request
     * @return Response
     * @Route("/api/addtheme", options={"expose"=true}, name="addtheme")
     * @Method("POST")
     */
    public function addTheme(Request $request){

        if ($request->isXMLHttpRequest()) {
            $session = $request->getSession();
            if(!$session->has('userInfo') || $session->get('userInfo')['profile']->getRole() != 'ADMINISTRATOR'){
                $error = ['title' => 'Une erreur est survenue', 'error' => ''];
                return new JsonResponse($error);
            }

            $data['title'] = $request->get('title');
            $data['subtitle'] = $request->get('subtitle');
            $data['category'] = $request->get('category');
            $data['picture_base64'] = $request->get('picture_base64');
            $data['header'] = $request->get('header');
            $data['souscategory'] = $request->get('souscategory');
            $data['type'] = $request->get('type');
            $data['url'] = $request->get('url');
            $data['expiration_date'] = $request->get('expiration_date');

            if(!isset($data['title']) || $data['title'] == ''
                || !isset($data['category']) || $data['category'] == ''
                || !isset($data['souscategory']) || $data['souscategory'] == ''
                || !isset($data['type']) || $data['type'] == ''){
                $error = ['title' => 'Information manquante', 'error' => 'Veuillez remplir tous les champs'];
                return new JsonResponse($error);
            }

            if($data['picture_base64'] != ''){
                $img = $data['picture_base64'];
                $img = str_replace('data:image/png;base64,', '', $img);
                $img = str_replace('data:image/jpeg;base64,', '', $img);
                $img = str_replace(' ', '+', $img);
                $datab64= base64_decode($img);
                $filename = uniqid() . '.png';
                $file = 'img/theme_assets/' . $filename;
                $success = file_put_contents($file, $datab64);

                if(!$success){
                    $error = ['title' => 'Erreur', 'error' => 'Une erreur est survenue'];
                    return new JsonResponse($error);
                }
            }

            if(isset($_FILES["file"])){
                if ($_FILES["file"]["size"] > 50000000) {
                    $error = ['title' => 'Erreur', 'error' => 'Veuillez ajouter une vidéo de moins de 50Mo'];
                    return new JsonResponse($error);
                }
                $imageFileType = strtolower(pathinfo(basename($_FILES["file"]["name"]),PATHINFO_EXTENSION));
                if($imageFileType != "mp4") {
                    $error = ['title' => 'Erreur', 'error' => 'Veuillez ajouter une vidéo au format .MP4'];
                    return new JsonResponse($error);
                }

                $filename = uniqid() . '.mp4';
                $file = 'img/theme_assets/' . $filename;
                if (!move_uploaded_file($_FILES["file"]["tmp_name"], $file)) {
                    $error = ['title' => 'Erreur', 'error' => 'Une erreur est survenue'];
                    return new JsonResponse($error);
                }
                $data['url'] = $file;
            }

            $em = $this->getDoctrine()->getManager();

            /*
            *
            *  SAVE THEME HERE
            *
            */

            $themeList = $em->getRepository('AppBundle:Themes')->findBy([
                'category' => $data['category'],
                'souscategory' => $data['souscategory']
            ], [
                'order' => 'DESC'
            ], 1);

            $orderNumber = 0;
            if(isset($themeList[0])){
                $orderNumber = $themeList[0]->getOrder();
            }

            $theme = new Themes();
            $theme->setTitle($data['title']);
            $theme->setSubtitle($data['subtitle']);
            $theme->setVisible(true);
            $theme->setType($data['type']);
            $theme->setCategory($data['category']);
            $theme->setSouscategory($data['souscategory']);
            $theme->setHeader($data['header']);
            $theme->setUrl($data['url']);
            $theme->setOrder($orderNumber + 1);
            if(isset($filename))
                $theme->setPicture('/img/theme_assets/' . $filename);

            if($data['type'] == 'video' || $data['type'] == 'external-video'){
                if($request->get('expiration_date') != ''){
                    $theme->setExpirationDate(\DateTime::createFromFormat('d/m/Y', $data['expiration_date']));
                }else{
                    $theme->setExpirationDate(null);
                }
            }

            $em->persist($theme);

            $em->flush();


            $success = ["success" => true];
            return new JsonResponse($success);
        }
        return new Response('BAD REQUEST!', 400);
    }

    /**
     * @param Request $request
     * @return Response
     * @Route("/api/addsoustheme", options={"expose"=true}, name="addsoustheme")
     * @Method("POST")
     */
    public function addSousTheme(Request $request){

        if ($request->isXMLHttpRequest()) {
            $session = $request->getSession();
            if(!$session->has('userInfo') || $session->get('userInfo')['profile']->getRole() != 'ADMINISTRATOR'){
                $error = ['title' => 'Une erreur est survenue', 'error' => ''];
                return new JsonResponse($error);
            }

            $data['title'] = $request->get('title');
            $data['theme_id'] = $request->get('theme_id');
            $data['picture_base64'] = $request->get('picture_base64');
            $data['content'] = $request->get('content');
            $data['picture_only'] = $request->get('picture_only');

            if(!isset($data['title']) || $data['title'] == ''
                || !isset($data['theme_id']) || $data['theme_id'] == ''
                || !isset($data['content']) || $data['content'] == ''
                || !isset($data['picture_only']) || $data['picture_only'] == ''){
                $error = ['title' => 'Information manquante', 'error' => 'Veuillez remplir tous les champs'];
                return new JsonResponse($error);
            }

            if($data['picture_base64'] != ''){
                define('UPLOAD_DIR', 'img/soustheme_assets/');
                $img = $data['picture_base64'];
                $img = str_replace('data:image/png;base64,', '', $img);
                $img = str_replace('data:image/jpeg;base64,', '', $img);
                $img = str_replace(' ', '+', $img);
                $datab64= base64_decode($img);
                $filename = uniqid() . '.png';
                $file = UPLOAD_DIR . $filename;
                $success = file_put_contents($file, $datab64);

                if(!$success){
                    $error = ['title' => 'Erreur', 'error' => 'Une erreur est survenue'];
                    return new JsonResponse($error);
                }
            }


            $em = $this->getDoctrine()->getManager();

            /*
            *
            *  SAVE SOUSTHEME HERE
            *
            */

            $theme = $em->getRepository('AppBundle:Themes')->find($data['theme_id']);

            if(!$theme){
                $error = ['title' => 'Erreur', 'error' => 'Une erreur est survenue'];
                return new JsonResponse($error);
            }
            $soustheme = new Sousthemes();
            $soustheme->setTitle($data['title']);
            $soustheme->setTheme($theme);
            if(isset($filename))
                $soustheme->setPicture('/img/soustheme_assets/' . $filename);

            if($data['picture_only'] === 'true'){
                $soustheme->setContent('');
                $soustheme->setPictureOnly(true);
            }else{
                $soustheme->setContent($data['content']);
                $soustheme->setPictureOnly(false);
            }

            $em->persist($soustheme);

            $em->flush();


            $success = ["success" => true];
            return new JsonResponse($success);
        }
        return new Response('BAD REQUEST!', 400);
    }

    /**
     * @param Request $request
     * @return Response
     * @Route("/api/modifytheme", options={"expose"=true}, name="modifytheme")
     * @Method("POST")
     */
    public function modifyTheme(Request $request){

        if ($request->isXMLHttpRequest()) {
            $session = $request->getSession();
            if(!$session->has('userInfo') || $session->get('userInfo')['profile']->getRole() != 'ADMINISTRATOR'){
                $error = ['title' => 'Une erreur est survenue', 'error' => ''];
                return new JsonResponse($error);
            }

            $data['title'] = $request->get('title');
            $data['subtitle'] = $request->get('subtitle');
            $data['category'] = $request->get('category');
            $data['picture_base64'] = $request->get('picture_base64');
            $data['header'] = $request->get('header');
            $data['souscategory'] = $request->get('souscategory');
            $data['type'] = $request->get('type');
            $data['url'] = $request->get('url');
            $data['theme_id'] = $request->get('theme_id');

            if($data['type'] == 'video'){
                if($request->get('expiration_date') != ''){
                    $data['expiration_date'] = $request->get('expiration_date');
                }
            }

            if(!isset($data['title']) || $data['title'] == ''
                || !isset($data['category']) || $data['category'] == ''
                || !isset($data['souscategory']) || $data['souscategory'] == ''
                || !isset($data['theme_id']) || $data['theme_id'] == ''
                || !isset($data['type']) || $data['type'] == ''){
                $error = ['title' => 'Information manquante', 'error' => 'Veuillez remplir tous les champs'];
                return new JsonResponse($error);
            }

            $img = $data['picture_base64'];
            $img = str_replace('data:image/png;base64,', '', $img);
            $img = str_replace('data:image/jpeg;base64,', '', $img);
            $img = str_replace(' ', '+', $img);

            if($img != '' && base64_encode(base64_decode($img, true)) === $img){
                define('UPLOAD_DIR', 'img/theme_assets/');

                $datab64= base64_decode($img);
                $filename = uniqid() . '.png';
                $file = UPLOAD_DIR . $filename;
                $success = file_put_contents($file, $datab64);

                if(!$success){
                    $error = ['title' => 'Erreur', 'error' => 'Une erreur est survenue'];
                    return new JsonResponse($error);
                }
            }

            if(isset($_FILES["file"])){
                if ($_FILES["file"]["size"] > 50000000) {
                    $error = ['title' => 'Erreur', 'error' => 'Veuillez ajouter une vidéo de moins de 50Mo'];
                    return new JsonResponse($error);
                }
                $imageFileType = strtolower(pathinfo(basename($_FILES["file"]["name"]),PATHINFO_EXTENSION));
                if($imageFileType != "mp4") {
                    $error = ['title' => 'Erreur', 'error' => 'Veuillez ajouter une vidéo au format .MP4'];
                    return new JsonResponse($error);
                }

                $filename = uniqid() . '.mp4';
                $file = 'img/theme_assets/' . $filename;
                if (!move_uploaded_file($_FILES["file"]["tmp_name"], $file)) {
                    $error = ['title' => 'Erreur', 'error' => 'Une erreur est survenue'];
                    return new JsonResponse($error);
                }
                $data['url'] = $file;
            }


            $em = $this->getDoctrine()->getManager();

            /*
            *
            *  SAVE THEME HERE
            *
            */

            $theme = $em->getRepository('AppBundle:Themes')->find($data['theme_id']);

            $theme->setTitle($data['title']);
            $theme->setSubtitle($data['subtitle']);
            $theme->setType($data['type']);
            $theme->setCategory($data['category']);
            $theme->setSouscategory($data['souscategory']);
            $theme->setHeader($data['header']);
            $theme->setUrl($data['url']);
            if(isset($filename))
                $theme->setPicture('/img/theme_assets/' . $filename);

            if($data['type'] != 'fiche'){
                $theme->setPicture('');
            }

            if($data['type'] == 'video'){
                if($request->get('expiration_date') != ''){
                    $theme->setExpirationDate(\DateTime::createFromFormat('d/m/Y', $data['expiration_date']));
                }else{
                    $theme->setExpirationDate(null);
                }
            }

            $em->persist($theme);

            $em->flush();


            $success = ["success" => true];
            return new JsonResponse($success);
        }
        return new Response('BAD REQUEST!', 400);
    }

    /**
     * @param Request $request
     * @return Response
     * @Route("/api/modifysoustheme", options={"expose"=true}, name="modifysoustheme")
     * @Method("POST")
     */
    public function modifySousTheme(Request $request){

        if ($request->isXMLHttpRequest()) {
            $session = $request->getSession();
            if(!$session->has('userInfo') || $session->get('userInfo')['profile']->getRole() != 'ADMINISTRATOR'){
                $error = ['title' => 'Une erreur est survenue', 'error' => ''];
                return new JsonResponse($error);
            }

            $data['title'] = $request->get('title');
            $data['content'] = $request->get('content');
            $data['picture_base64'] = $request->get('picture_base64');
            $data['soustheme_id'] = $request->get('soustheme_id');
            $data['theme_id'] = $request->get('theme_id');
            $data['picture_only'] = $request->get('picture_only');

            if(!isset($data['title']) || $data['title'] == ''
                || !isset($data['content']) || $data['content'] == ''
                || !isset($data['soustheme_id']) || $data['soustheme_id'] == ''
                || !isset($data['theme_id']) || $data['theme_id'] == ''
                || !isset($data['picture_only']) || $data['picture_only'] == ''){
                $error = ['title' => 'Information manquante', 'error' => 'Veuillez remplir tous les champs'];
                return new JsonResponse($error);
            }

            $img = $data['picture_base64'];
            $img = str_replace('data:image/png;base64,', '', $img);
            $img = str_replace('data:image/jpeg;base64,', '', $img);
            $img = str_replace(' ', '+', $img);

            if($img != '' && base64_encode(base64_decode($img, true)) === $img){
                define('UPLOAD_DIR', 'img/soustheme_assets/');

                $datab64= base64_decode($img);
                $filename = uniqid() . '.png';
                $file = UPLOAD_DIR . $filename;
                $success = file_put_contents($file, $datab64);

                if(!$success){
                    $error = ['title' => 'Erreur', 'error' => 'Une erreur est survenue'];
                    return new JsonResponse($error);
                }
            }


            $em = $this->getDoctrine()->getManager();

            /*
            *
            *  SAVE THEME HERE
            *
            */

            $soustheme = $em->getRepository('AppBundle:Sousthemes')->find($data['soustheme_id']);
            $theme = $em->getRepository('AppBundle:Themes')->find($data['theme_id']);

            if(!$soustheme || !$theme){
                $error = ['title' => 'Erreur', 'error' => 'Une erreur est survenue'];
                return new JsonResponse($error);
            }

            $soustheme->setTitle($data['title']);
            $soustheme->setTheme($theme);
            if(isset($filename))
                $soustheme->setPicture('/img/soustheme_assets/' . $filename);

            if($data['picture_only'] === 'true'){
                $soustheme->setPictureOnly(true);
                $soustheme->setContent('');
            }else{
                $soustheme->setContent($data['content']);
                $soustheme->setPictureOnly(false);
            }

            $em->persist($soustheme);
            $em->persist($theme);

            $em->flush();


            $success = ["success" => true];
            return new JsonResponse($success);
        }
        return new Response('BAD REQUEST!', 400);
    }

    /**
     * @param Request $request
     * @return Response
     * @Route("/api/moveuptheme", options={"expose"=true}, name="moveuptheme")
     * @Method("POST")
     */
    public function moveUpTheme(Request $request){

        if ($request->isXMLHttpRequest()) {
            $session = $request->getSession();
            if(!$session->has('userInfo') || $session->get('userInfo')['profile']->getRole() != 'ADMINISTRATOR'){
                $error = ['title' => 'Une erreur est survenue', 'error' => ''];
                return new JsonResponse($error);
            }

            $data['theme_id'] = $request->get('theme_id');

            if(!isset($data['theme_id']) || $data['theme_id'] == ''){
                $error = ['title' => 'Information manquante', 'error' => 'Veuillez remplir tous les champs'];
                return new JsonResponse($error);
            }

            $em = $this->getDoctrine()->getManager();

            /*
            *
            *  MOVE UP THEME HERE
            *
            */

            $theme = $em->getRepository('AppBundle:Themes')->find($data['theme_id']);

            $themeList = $em->getRepository('AppBundle:Themes')->findBy([
                'category' => $theme->getCategory(),
                'souscategory' => $theme->getSouscategory(),
            ], ['order' => 'ASC']);

            $order = 1;
            foreach($themeList as $t){
                $t->setOrder($order);
                $order++;
                $em->persist($t);
            }

            $em->flush();

            $themeToMove = $em->getRepository('AppBundle:Themes')->findOneBy([
                'category' => $theme->getCategory(),
                'souscategory' => $theme->getSouscategory(),
                'order' => ($theme->getOrder() - 1)
            ]);

            if($themeToMove){
                $theme->setOrder($theme->getOrder() - 1);
                $themeToMove->setOrder($theme->getOrder() + 1);

                $em->persist($theme);
                $em->persist($themeToMove);

                $em->flush();
            }else{
                $error = ['title' => '', 'error' => 'Vous êtes déjà au plus haut de la sous catégorie'];
                return new JsonResponse($error);
            }

            $success = ["success" => true];
            return new JsonResponse($success);
        }
        return new Response('BAD REQUEST!', 400);
    }

    /**
     * @param Request $request
     * @return Response
     * @Route("/api/movedowntheme", options={"expose"=true}, name="movedowntheme")
     * @Method("POST")
     */
    public function moveDownTheme(Request $request){

        if ($request->isXMLHttpRequest()) {
            $session = $request->getSession();
            if(!$session->has('userInfo') || $session->get('userInfo')['profile']->getRole() != 'ADMINISTRATOR'){
                $error = ['title' => 'Une erreur est survenue', 'error' => ''];
                return new JsonResponse($error);
            }

            $data['theme_id'] = $request->get('theme_id');

            if(!isset($data['theme_id']) || $data['theme_id'] == ''){
                $error = ['title' => 'Information manquante', 'error' => 'Veuillez remplir tous les champs'];
                return new JsonResponse($error);
            }

            $em = $this->getDoctrine()->getManager();



            /*
            *
            *  MOVE UP THEME HERE
            *
            */

            $theme = $em->getRepository('AppBundle:Themes')->find($data['theme_id']);

            $themeList = $em->getRepository('AppBundle:Themes')->findBy([
                'category' => $theme->getCategory(),
                'souscategory' => $theme->getSouscategory(),
            ], ['order' => 'ASC']);

            $order = 1;
            foreach($themeList as $t){
                $t->setOrder($order);
                $order++;
                $em->persist($t);
            }

            $em->flush();

            $themeToMove = $em->getRepository('AppBundle:Themes')->findOneBy([
                'category' => $theme->getCategory(),
                'souscategory' => $theme->getSouscategory(),
                'order' => ($theme->getOrder() + 1)
            ]);

            if($themeToMove){
                $theme->setOrder($theme->getOrder() + 1);
                $themeToMove->setOrder($theme->getOrder() - 1);

                $em->persist($theme);
                $em->persist($themeToMove);

                $em->flush();
            }else{
                $error = ['title' => '', 'error' => 'Vous êtes déjà au plus bas de la sous catégorie'];
                return new JsonResponse($error);
            }

            $success = ["success" => true];
            return new JsonResponse($success);
        }
        return new Response('BAD REQUEST!', 400);
    }

    /**
     * @param Request $request
     * @return Response
     * @Route("/api/deletetheme", options={"expose"=true}, name="deletetheme")
     * @Method("POST")
     */
    public function deleteTheme(Request $request){

        if ($request->isXMLHttpRequest()) {
            $session = $request->getSession();
            if(!$session->has('userInfo') || $session->get('userInfo')['profile']->getRole() != 'ADMINISTRATOR'){
                $error = ['error' => 'Une erreur est survenue', 'msg' => ''];
                return new JsonResponse($error);
            }

            $data['theme_id'] = $request->get('theme_id');

            if(!isset($data['theme_id']) || $data['theme_id'] == ''){
                $error = ['title' => 'Information manquante', 'error' => 'Veuillez remplir tous les champs'];
                return new JsonResponse($error);
            }

            $em = $this->getDoctrine()->getManager();

            /*
            *
            *  DELETE THEME HERE
            *
            */

            $theme = $em->getRepository('AppBundle:Themes')->find($data['theme_id']);

            if (!$theme) {
                $error = ['title' => 'Erreur', 'error' => 'Theme non trouvé'];
                return new JsonResponse($error);
            }

            $em->remove($theme);
            $em->flush();

            $success = ["success" => true];
            return new JsonResponse($success);
        }
        return new Response('BAD REQUEST!', 400);
    }

    /**
     * @param Request $request
     * @return Response
     * @Route("/api/deletesoustheme", options={"expose"=true}, name="deletesoustheme")
     * @Method("POST")
     */
    public function deleteSousTheme(Request $request){

        if ($request->isXMLHttpRequest()) {
            $session = $request->getSession();
            if(!$session->has('userInfo') || $session->get('userInfo')['profile']->getRole() != 'ADMINISTRATOR'){
                $error = ['error' => 'Une erreur est survenue', 'msg' => ''];
                return new JsonResponse($error);
            }

            $data['soustheme_id'] = $request->get('soustheme_id');

            if(!isset($data['soustheme_id']) || $data['soustheme_id'] == ''){
                $error = ['title' => 'Information manquante', 'error' => 'Veuillez remplir tous les champs'];
                return new JsonResponse($error);
            }

            $em = $this->getDoctrine()->getManager();

            /*
            *
            *  DELETE SOUSTHEME HERE
            *
            */

            $soustheme = $em->getRepository('AppBundle:Sousthemes')->find($data['soustheme_id']);

            if (!$soustheme) {
                $error = ['title' => 'Erreur', 'error' => 'Article non trouvé'];
                return new JsonResponse($error);
            }

            $em->remove($soustheme);
            $em->flush();

            $success = ["success" => true];
            return new JsonResponse($success);
        }
        return new Response('BAD REQUEST!', 400);
    }

    /**
     * @param Request $request
     * @return Response
     * @Route("/api/updateinformation", options={"expose"=true}, name="updateinformation")
     * @Method("POST")
     */
    public function updateInformation(Request $request){

        if ($request->isXMLHttpRequest()) {
            $session = $request->getSession();
            if(!$session->has('userInfo') || $session->get('userInfo')['profile']->getRole() != 'ADMINISTRATOR'){
                $error = ['error' => 'Une erreur est survenue', 'msg' => ''];
                return new JsonResponse($error);
            }

            $data['hideRatingAfterMark'] = $request->get('hideRatingAfterMark');
            $data['trWebsiteAccueil'] = $request->get('trWebsiteAccueil');
            $data['trConseillerPhone'] = $request->get('trConseillerPhone');
            $data['trConseillerMailDirect'] = $request->get('trConseillerMailDirect');
            $data['trConseillerMailCallback'] = $request->get('trConseillerMailCallback');
            $data['trMessageCallConseiller'] = $request->get('trMessageCallConseiller');
            $data['generalCondition'] = $request->get('generalCondition');


            if(!isset($data['trWebsiteAccueil']) || $data['trWebsiteAccueil'] == '' ||
                !isset($data['trConseillerPhone']) || $data['trConseillerPhone'] == '' ||
                !isset($data['trConseillerMailDirect']) || $data['trConseillerMailDirect'] == '' ||
                !isset($data['trConseillerMailCallback']) || $data['trConseillerMailCallback'] == '' ||
                !isset($data['trMessageCallConseiller']) || $data['trMessageCallConseiller'] == '' ||
                !isset($data['generalCondition']) || $data['generalCondition'] == '' ||
                !isset($data['hideRatingAfterMark']) || $data['hideRatingAfterMark'] == ''){


                $error = ['title' => 'INFORMATION MANQUANTE', 'error' => 'Veuillez remplir tous les champs'];
                return new JsonResponse($error);
            }


            $em = $this->getDoctrine()->getManager();

            $appInfo = $em->getRepository('AppBundle:Appinfo')
                ->find(1);

            $appInfo->setTrWebsiteAccueil($data['trWebsiteAccueil']);
            $appInfo->setTrConseillerPhone($data['trConseillerPhone']);
            $appInfo->setTrConseillerMailDirect($data['trConseillerMailDirect']);
            $appInfo->setTrConseillerMailCallback($data['trConseillerMailCallback']);
            $appInfo->setTrMessageCallConseiller($data['trMessageCallConseiller']);
            $appInfo->setGeneralCondition($data['generalCondition']);
            $appInfo->setHideRatingAfterMark($data['hideRatingAfterMark']);

            $em->flush();


            $success = ["success" => true];
            return new JsonResponse($success);
        }
        return new Response('BAD REQUEST!', 400);
    }

    /**
     * @param Request $request
     * @Route("/api/getclient", options={"expose"=true}, name="getclient")
     * @Method("POST")
     * @return Response
     */
    public function getClient(Request $request){

        if ($request->isXMLHttpRequest()) {
            $session = $request->getSession();
            if(!$session->has('userInfo') || $session->get('userInfo')['profile']->getRole() != 'ADMINISTRATOR'){
                $error = ['error' => 'Une erreur est survenue', 'msg' => ''];
                return new JsonResponse($error);
            }

            $data['id'] = $request->get('id');

            if(!isset($data['id']) || $data['id'] == ''){
                $error = ['title' => 'INFORMATION MANQUANTE', 'error' => 'Veuillez renseigner toutes les informations nécessaires'];
                return new JsonResponse($error);
            }

            $em = $this->getDoctrine()->getManager();


            $user = $em->getRepository('AppBundle:Users')
                ->find($data['id']);

            $content = [];

            $content['template_client_unique'] = $this->render('ajax/updateclient.html.twig', [
                'client' => $user,
            ])->getContent();

            return new JsonResponse($content);
        }
        return new Response('BAD REQUEST!', 400);
    }

    /**
     * @param Request $request
     * @return Response
     * @Route("/api/updateclient", options={"expose"=true}, name="updateclient")
     * @Method("POST")
     */
    public function updateClient(Request $request){

        if ($request->isXMLHttpRequest()) {
            $session = $request->getSession();
            if(!$session->has('userInfo') || $session->get('userInfo')['profile']->getRole() != 'ADMINISTRATOR'){
                $error = ['error' => 'Une erreur est survenue', 'msg' => ''];
                return new JsonResponse($error);
            }

            $data['id'] = $request->get('id');
            $data['firstname'] = $request->get('firstname');
            $data['lastname'] = $request->get('lastname');
            $data['phone'] = $request->get('phone');
            $data['email_status'] = $request->get('email_status');
            $data['subscription_status'] = $request->get('subscription_status');
            $data['subscription_createdAt'] = $request->get('subscription_createdAt');

            if(!isset($data['firstname']) || $data['firstname'] == ''
                || !isset($data['id']) || $data['id'] == ''
                || !isset($data['lastname']) || $data['lastname'] == ''
                || !isset($data['email_status']) || $data['email_status'] == ''
                || !isset($data['subscription_status']) || $data['subscription_status'] == ''
                || !isset($data['subscription_createdAt']) || $data['subscription_createdAt'] == ''){
                $error = ['error' => 'Information manquante', 'msg' => 'Veuillez remplir tous les champs'];
                return new JsonResponse($error);
            }

            if($data['phone'] != '' && preg_match('/^(?:(?:\+|00)33|0)\s*[1-9](?:[\s.-]*\d{2}){4}$/', $data['phone']) != 1){
                $error = ['error' => 'Information Erronée', 'msg' => 'Veuillez saisir un numéro valide'];
                return new JsonResponse($error);
            }

            $em = $this->getDoctrine()->getManager();

            $user = $em->getRepository('AppBundle:Users')
                ->find($data['id']);

            $user->setFirstname($data['firstname']);
            $user->setLastname($data['lastname']);
            $user->setPhone($data['phone']);
            $user->setStatus($data['email_status']);

            $subscription = $em->getRepository('AppBundle:Subscription')
                ->findOneBy(['user' => $user]);
            $subscription->setStatus($data['subscription_status']);
            $subscription->setCreatedAt(\DateTime::createFromFormat('d/m/Y', $data['subscription_createdAt']));
            $em->flush();


            $success = ["success" => true];
            return new JsonResponse($success);
        }
        return new Response('BAD REQUEST!', 400);
    }

    /**
     * @Route("/api/extract", options={"expose"=true}, name="extract")
     * @param Request $request
     * @return JsonResponse
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    public function exportAction(Request $request){
        $session = $request->getSession();
        if(!$session->has('userInfo') || $session->get('userInfo')['profile']->getRole() != 'ADMINISTRATOR'){
            $error = ['error' => 'Une erreur est survenue', 'msg' => ''];
            return new JsonResponse($error);
        }

        error_reporting(E_ALL);
        ini_set('display_errors', TRUE);
        ini_set('display_startup_errors', TRUE);
        date_default_timezone_set('Europe/Paris');

        if (PHP_SAPI == 'cli')
            die('This example should only be run from a Web Browser');

//        /** Include PHPExcel */
//        require_once $_SERVER['DOCUMENT_ROOT'].'/php/PHPExcel.php';

        $title = 'Extraction';
        $date = date('Ymd');

        $em = $this->getDoctrine()->getManager();

        $clients = $em->getRepository('AppBundle:Users')
            ->getClients();

        // Create new PHPExcel object
        $objPHPExcel = new Spreadsheet();

        // Set document properties
        $objPHPExcel->getProperties()->setCreator("Stream-Techs")
            ->setLastModifiedBy("Stream-Techs")
            ->setTitle($title)
            ->setSubject($title)
            ->setDescription("Extraction")
            ->setKeywords("Extraction")
            ->setCategory("Extraction");

        // Add some data
        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', '#')
            ->setCellValue('B1', 'Prénom')
            ->setCellValue('C1', 'Nom')
            ->setCellValue('D1', 'Email')
            ->setCellValue('E1', 'Téléphone')
            ->setCellValue('F1', 'Date de souscription')
            ->setCellValue('G1', 'Status Email')
            ->setCellValue('H1', 'Status Paiement')
            ->setCellValue('I1', 'Token')
            ->setCellValue('J1', 'Téléphone 2');

        $styleArray = array(
            'borders' => array(
                'outline' => array(
                    'borderStyle' => Border::BORDER_THIN,
                    'color' => array('argb' => '2c3e50'),
                ),
            ),
            'fill' => array(
                'fillType' => Fill::FILL_SOLID,
                'color' => array('rgb' => '2c3e50')
            ),
            'font' => array(
                'color' => array('rgb' => 'FFFFFF'),
                'bold' => true
            ),
        );
        $objPHPExcel->getActiveSheet()->getStyle('A1:J1')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setAutoFilter('A1:J1');


        $cpt = 1;
//        Les resultats de la requette
        foreach($clients as $client){
            $cpt=$cpt+1;
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A'.$cpt, $client->getId())
                ->setCellValue('B'.$cpt, $client->getFirstname())
                ->setCellValue('C'.$cpt, $client->getLastname())
                ->setCellValue('D'.$cpt, $client->getEmail())
                ->setCellValue('E'.$cpt, $client->getPhone())
                ->setCellValue('F'.$cpt, $client->getSubscription()[0]->getCreatedAt())
                ->setCellValue('G'.$cpt, $client->getStatus())
                ->setCellValue('H'.$cpt, $client->getSubscription()[0]->getStatus())
                ->setCellValue('I'.$cpt, $client->getToken());
//                ->setCellValue('J'.$cpt, $client->getPhone2());
        }

        // Rename worksheet
        $objPHPExcel->getActiveSheet()->setTitle($title);
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);
        // Redirect output to a client’s web browser (Excel2007)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Extraction_'.$date.'.xlsx"');
        header('Cache-Control: max-age=0');

        $objWriter = IOFactory::createWriter($objPHPExcel, 'Xlsx');
        $objWriter->save('php://output');
        exit;
    }


    /**
     * @Route("/api/cronexport", options={"expose"=true}, name="cronexport")
     * @param Request $request
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    public function cronexportAction(Request $request){

        error_reporting(E_ALL);
        ini_set('display_errors', TRUE);
        ini_set('display_startup_errors', TRUE);
        date_default_timezone_set('Europe/Paris');

        if (PHP_SAPI == 'cli')
            die('This example should only be run from a Web Browser');

//        /** Include PHPExcel */
//        require_once $_SERVER['DOCUMENT_ROOT'].'/php/PHPExcel.php';

        $title = 'MACIF_CN_'.date('Y-m-d');
        $date = date('Ymd');

        $em = $this->getDoctrine()->getManager();

        $stats = $em->getRepository('AppBundle:Statistics')->getExportStats();
        // Create new PHPExcel object
        $objPHPExcel = new Spreadsheet();

        // Set document properties
        $objPHPExcel->getProperties()->setCreator("Stream-Techs")
            ->setLastModifiedBy("Stream-Techs")
            ->setTitle($title)
            ->setSubject($title)
            ->setDescription("Extraction")
            ->setKeywords("Extraction")
            ->setCategory("Extraction");

        // Add some data
        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'action_name')
            ->setCellValue('B1', 'triggered_at')
            ->setCellValue('C1', 'firstname')
            ->setCellValue('D1', 'lastname')
            ->setCellValue('E1', 'email')
            ->setCellValue('F1', 'phone')
            ->setCellValue('G1', 'birth_date')
            ->setCellValue('H1', 'Nr_Societaire')
            ->setCellValue('I1', 'idQuizzMacif')
            ->setCellValue('J1', 'content_id')
            ->setCellValue('K1', 'content_title')
            ->setCellValue('L1', 'content_type');

        $styleArray = array(
            'borders' => array(
                'outline' => array(
                    'borderStyle' => Border::BORDER_THIN,
                    'color' => array('rgb' => '2c3e50')
                ),
            ),
            'fill' => array(
                'fillType' => Fill::FILL_SOLID,
                'color' => array('rgb' => '2c3e50')
            ),
            'font' => array(
                'color' => array('rgb' => 'FFFFFF'),
                'bold' => true
            ),
        );
        $objPHPExcel->getActiveSheet()->getStyle('A1:L1')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setAutoFilter('A1:L1');


        $cpt = 1;
//        Les resultats de la requette
        foreach($stats as $stat){
            $cpt=$cpt+1;

            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A'.$cpt, $stat['action_name'])
                ->setCellValue('B'.$cpt, $stat['triggered_at'])
                ->setCellValue('C'.$cpt, $stat['firstname'])
                ->setCellValue('D'.$cpt, $stat['lastname'])
                ->setCellValue('E'.$cpt, $stat['email'])
                ->setCellValue('F'.$cpt, $stat['phone'])
                ->setCellValue('G'.$cpt, $stat['birth_date'])
                ->setCellValue('H'.$cpt, $stat['member_num'])
                ->setCellValue('I'.$cpt, $stat['id_quizz_macif'])
                ->setCellValue('J'.$cpt, $stat['content_id'])
                ->setCellValue('K'.$cpt, $stat['content_title'])
                ->setCellValue('L'.$cpt, $stat['content_type'])
            ;
        }

        // Rename worksheet
        $objPHPExcel->getActiveSheet()->setTitle($title);
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);
        //--------------------------------------------------------------------------------------------------------------
        //--------------------------------------------------------------------------------------------------------------
        $objWriter = IOFactory::createWriter($objPHPExcel, 'Xlsx');
        $objWriter->save(str_replace(__FILE__,$this->getParameter('export_location').$title.'.xlsx',__FILE__));
        exit;
    }
}