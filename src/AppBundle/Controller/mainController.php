<?php
/**
 * Created by PhpStorm.
 * User: yr
 * Date: 17/11/2017
 * Time: 01:54
 */

namespace AppBundle\Controller;


use AppBundle\Entity\Statistics;
use AppBundle\Entity\Subscription;
use AppBundle\Entity\Users;
use AppBundle\Utils\Utils;
use GuzzleHttp\Client;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;


class mainController extends Controller
{
    /**
     * @param string $actionName
     * @param int $userId
     */
    private function recordAction($actionName, $userId = null){
        $em = $this->getDoctrine()->getManager();


        $stat = new Statistics();
        if($userId){
            $user = $em->getRepository(Users::class)->find($userId);
            if($user){
                $stat->setUser($user);
            }
        }
        $stat->setActionName($actionName);
        $em->persist($stat);
        $em->flush();
    }

    /**
     * @Route("/", name="macifRedirect")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function macifRedirectAction(Request $request){

        $this->recordAction('MACIF_SSO_1_REDIRECT_TO_LOGIN');

        $baseUrl = $this->getParameter('macif_sso_authorization_url');

        $params = [
            'response_type' => $this->getParameter('macif_sso_response_type'),
            'client_id' => $this->getParameter('macif_sso_client_id'),
            'redirect_uri' => $this->getParameter('macif_sso_redirect_uri'),
            'scope' => $this->getParameter('macif_sso_scope'),
            'pfidpadapterid' => $this->getParameter('macif_sso_pfidpadapterid')
        ];


        $utils = new Utils();

        $nonce = $utils->generateOneDayHash($this->getParameter('secret'));
        $params['nonce'] = $nonce;

        $state = $utils->generateState($nonce, $this->getParameter('secret'));
        $params['state'] = $state;


        $queryString = $utils->createQueryString($params);

        $redirectUrl = $baseUrl . '?' . $queryString;

        return $this->redirect($redirectUrl);
    }

    /**
     * @Route("/connect", name="macifConnect")
     * @param Request $request
     * @return Response
     * @throws \Exception
     */
    public function macifConnectAction(Request $request){

        $utils = new Utils();

        // Check if error key exists or query params are missing

        if(array_key_exists('error', $_GET)
            || !array_key_exists('state', $_GET)
            || !array_key_exists('code', $_GET)
            || !$utils->checkStateIsValid(
                $utils->generateOneDayHash($this->getParameter('secret')),
                $this->getParameter('secret'),
                $_GET['state']
            )
        ){
            $this->recordAction('MACIF_SSO_2_AUTH_ERROR');
            $message = (isset($_GET['error'])) ? $_GET['error'] : 'autorisation manquante';
            return $this->render('main/error.html.twig', ['message' => $message]);
        }

        $this->recordAction('MACIF_SSO_2_AUTH_SUCCESS');

        // Request ID Token to Macif Connect

        $client = new Client([ 'base_uri' => '', 'timeout' => 2.0 ]);

        $this->recordAction('MACIF_SSO_3_FETCH_INFO_REQUEST');

        try{

            $response = $client->post($this->getParameter('macif_sso_token_url'), [
                'form_params' => [
                    'grant_type' => 'authorization_code',
                    'code' => $_GET['code'],
                    'redirect_uri' => $this->getParameter('macif_sso_redirect_uri'),
                    'client_id' => $this->getParameter('macif_sso_client_id'),
                    'client_secret' => $this->getParameter('macif_sso_secret')
                ]
            ]);
        }catch(\Exception $e){
            // If Macif connect respond a 4XX error, then ...
            $this->recordAction('MACIF_SSO_4_FETCH_INFO_REQUEST_ERROR');
            $message = 'Code d\'autorisation invalide';
            return $this->render('main/error.html.twig', ['message' => $message]);
        }

        // If Macif connect respond a 4XX error, then ...

        if($response->getStatusCode() >= 400){
            $this->recordAction('MACIF_SSO_4_FETCH_INFO_REQUEST_ERROR');
            $message = 'Code d\'autorisation invalide';
            return $this->render('main/error.html.twig', ['message' => $message]);
        }

        // Check that all keys needed are received in response

        $keyNeeded = ['access_token', 'id_token', 'token_type', 'expires_in'];
        $arrayBody = json_decode($response->getBody()->getContents(), true);
        foreach($keyNeeded as $key){
            if(!array_key_exists($key, $arrayBody)){
                $message = 'Clé manquante<br/><br/><em>'.$response->getBody()->getContents().'</em>';
                return $this->render('main/error.html.twig', ['message' => $message]);
            }
        }

        // Decode the ID Token to get user data

        $decodedIdToken = $utils->jwtDecodeWithoutKey($arrayBody['id_token']);

        // Check the nonce validity

        if(!isset($decodedIdToken['nonce']) || !$utils->checkNonceIsValid($this->getParameter('secret'), $decodedIdToken['nonce'])){
            $this->recordAction('MACIF_SSO_4_FETCH_INFO_REQUEST_ILLEGAL');
            $message = 'Token invalide';
            return $this->render('main/error.html.twig', ['message' => $message]);
        }

        $this->recordAction('MACIF_SSO_4_FETCH_INFO_SUCCESS');

        $em = $this->getDoctrine()->getManager();
        $appInfo = $em->getRepository('AppBundle:Appinfo')->find(1);


        // Check if the user already exists in database
        $user = $em->getRepository('AppBundle:Users')->findOneBy(['email' => $decodedIdToken['znEmail']]);

        if(!$user){
            $user = $em->getRepository('AppBundle:Users')->findOneBy(['externalId' => $decodedIdToken['sub']]);
        }

        if(!$user){
            // Create user if not exists
            $user = new Users();
            $user->setFirstname($decodedIdToken['znPrenPers']);
            $user->setLastname($decodedIdToken['nmNaisPers']);
            $user->setEmail($decodedIdToken['znEmail']);
            $user->setMemberNum($decodedIdToken['noSoc']);
            $user->setPhone($decodedIdToken['noTelsSoc']);
            $user->setBirthDate(new \DateTime($decodedIdToken['dtNaisPers']));
            $user->setToken(bin2hex(random_bytes(20)));
            $user->setRole('CLIENT');
            $user->setStatus('VALIDATED');
            $user->setExternalId($decodedIdToken['sub']);


            $em->persist($user);

            $subscription = new Subscription();
            $date = new \DateTime();
            $interval = new \DateInterval('P1Y');
            $date->add($interval);
            $subscription->setEndContractAt($date);
            $subscription->setStatus('VALIDATED');
            $subscription->setAmountPaid($appInfo->getSubscriptionPrice());
            $subscription->setUser($user);

            $em->persist($subscription);

            $quizz = $em->getRepository('AppBundle:Quizz')->findOneBy(['user' => null]);
            $quizz->setUser($user);

            $em->persist($quizz);

            $stat = new Statistics();
            $stat->setActionName('INSCRIPTION');
            $stat->setUser($user);
            $em->persist($stat);

            $em->flush();

            $em->refresh($user);

            $this->recordAction('MACIF_SSO_5_CREATE_USER');
        }else{
            // Update external macif id
            $user->setBirthDate(new \DateTime($decodedIdToken['dtNaisPers']));
            $user->setExternalId($decodedIdToken['sub']);
            $em->persist($user);

            $em->flush();

            $em->refresh($user);

            $this->recordAction('MACIF_SSO_5_USER_ALREADY_EXISTS');
        }

        $session = new Session();

        $mem_email = false;
        $stay_connected = false;


        if($user->getRole() === 'CLIENT'){
            $data = [
                'profile' => $user,
                'mem_email' => $mem_email,
                'quizz_id' => $user->getQuizz()[0]->getCustomId(),
                'quizz001' => $user->getQuizz()[0]->getQuizz001(),
                'quizz004' => $user->getQuizz()[0]->getQuizz004(),
                'quizz006' => $user->getQuizz()[0]->getQuizz006(),
                'quizz008' => $user->getQuizz()[0]->getQuizz008(),
                'quizz007' => $user->getQuizz()[0]->getQuizz007(),
                'quizz009' => $user->getQuizz()[0]->getQuizz009(),
                'quizz011' => $user->getQuizz()[0]->getQuizz011(),
                'quizz012' => $user->getQuizz()[0]->getQuizz012(),
                'stay_connected' => $stay_connected
            ];
        }else{
            $data = [
                'profile' => $user,
                'mem_email' => $mem_email,
                'stay_connected' => $stay_connected
            ];
        }
        $session->set('userInfo', $data);


        $this->recordAction('MACIF_SSO_6_USER_CONNECTED_SUCCESSFULLY');

        return $this->redirectToRoute('auth');

    }

    /**
     * @Route("/theme/{category}", name="theme")
     * @param Request $request
     * @param string $category
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function themeAction(Request $request, $category){

        $session = $request->getSession();
        if(!$session->has('userInfo')){
            return $this->redirectToRoute('auth');
        }

        $em = $this->getDoctrine()->getManager();
        $appInfo = $em->getRepository('AppBundle:Appinfo')->find(1);

        if($category == null || ($category != 'fragilite' && $category != 'relation' && $category != 'aleas' && $category != 'equilibre')){
            return $this->redirectToRoute('auth');
        }

        if($category == 'fragilite'){
            $title = 'Surmonter les fragilités financières';
            $icone = '/img/global_design/icon/euro.png';
        }else if($category == 'relation'){
            $title = 'Gérer les relations aidants - aidés';
            $icone = '/img/global_design/icon/people.png';
        }else if ($category == 'aleas'){
            $title = 'Faire face aux aléas de la vie';
            $icone = '/img/global_design/icon/hands.png';
        }else if ($category == 'equilibre'){
            $title = 'Équilibrer santé et activité';
            $icone = '/img/global_design/icon/balance.png';
        }

        $themes = $em->getRepository('AppBundle:Themes')->findBy(['category' => $category, 'visible' => true],['order' => 'ASC', 'id' => 'ASC']);


        $response = $this->render('main/themes.html.twig', [
            'appinfo' => $appInfo,
            'userInfo' => $session->get('userInfo'),
            'themes' => $themes,
            'title' => $title,
            'icone' => $icone,
            'category' => $category
        ]);

        return $response;

    }

    /**
     * @Route("/soustheme/{theme_id}", name="soustheme")
     * @param Request $request
     * @param integer $theme_id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function sousthemeAction(Request $request, $theme_id){

        $session = $request->getSession();
        if(!$session->has('userInfo')){
            return $this->redirectToRoute('auth');
        }

        $em = $this->getDoctrine()->getManager();
        $appInfo = $em->getRepository('AppBundle:Appinfo')->find(1);

        if($theme_id == null || $theme_id == ''){
            return $this->redirectToRoute('auth');
        }

        $theme = $em->getRepository('AppBundle:Themes')->find($theme_id);

        if(!$theme){
            return $this->redirectToRoute('auth');
        }


        $response = $this->render('main/sousthemes.html.twig', [
            'appinfo' => $appInfo,
            'userInfo' => $session->get('userInfo'),
            'theme' => $theme
        ]);

        return $response;

    }

    /**
     * @Route("/article/{soustheme_id}", name="article")
     * @param Request $request
     * @param integer $soustheme_id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function articleAction(Request $request, $soustheme_id){

        $session = $request->getSession();
        if(!$session->has('userInfo')){
            return $this->redirectToRoute('auth');
        }

        $em = $this->getDoctrine()->getManager();
        $appInfo = $em->getRepository('AppBundle:Appinfo')->find(1);

        if($soustheme_id == null || $soustheme_id == ''){
            return $this->redirectToRoute('auth');
        }

        $soustheme = $em->getRepository('AppBundle:Sousthemes')->find($soustheme_id);

        if(!$soustheme){
            return $this->redirectToRoute('auth');
        }


        $response = $this->render('main/article.html.twig', [
            'appinfo' => $appInfo,
            'userInfo' => $session->get('userInfo'),
            'soustheme' => $soustheme
        ]);

        return $response;

    }

    /**
     * @Route("/wysiwyg/stksolutions_d41d8cd98f00b204e9800998ecf8427e/{soustheme_id}", name="wysiwyg")
     * @param Request $request
     * @param integer $soustheme_id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function wysiwygAction(Request $request, $soustheme_id){

        $session = $request->getSession();
        if(!$session->has('userInfo') || $session->get('userInfo')['profile']->getRole() != 'ADMINISTRATOR'){
            return $this->redirectToRoute('auth');
        }

        $em = $this->getDoctrine()->getManager();
        $appInfo = $em->getRepository('AppBundle:Appinfo')->find(1);

        if($soustheme_id == null || $soustheme_id == ''){
            return $this->redirectToRoute('auth');
        }

        if($request->isMethod('POST')){


            $em = $this->getDoctrine()->getManager();

            if(isset($_POST['soustheme_content'])){


                $soustheme = $em->getRepository('AppBundle:Sousthemes')->find($soustheme_id);

                $soustheme->setContent($_POST['soustheme_content']);

                $em->flush();
            }

        }


        $soustheme = $em->getRepository('AppBundle:Sousthemes')->find($soustheme_id);

        if(!$soustheme){
            return $this->redirectToRoute('auth');
        }

        $response = $this->render('main/wysiwyg.html.twig', [
            'appinfo' => $appInfo,
            'userInfo' => $session->get('userInfo'),
            'soustheme' => $soustheme
        ]);

        return $response;

    }

    /**
     * @Route("/inscription")
     */
    public function insciptionAction(){

        return $this->render('main/inscription.html.twig');
    }

    /**
     * @Route("/compte", name="compte")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function compteAction(Request $request){
        $session = $request->getSession();
        if(!$session->has('userInfo') || $session->get('userInfo')['profile']->getRole() != 'CLIENT'){
            return $this->redirectToRoute('auth');
        }

        $this->recordAction('PAGE_COMPTE', $session->get('userInfo')['profile']->getId());

        return $this->render('main/compte.html.twig',[
            'userInfo' => $session->get('userInfo'),
        ]);
    }

    /**
     * @Route("/password", name="password")
     */
    public function passwordAction(){

        return $this->render('main/password.html.twig');
    }

    /**
     * @Route("/changeuser/{user_id}", name="changeuser")
     * @param Request $request
     * @param $user_id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function changeuserAction(Request $request, $user_id){

        $session = $request->getSession();

        if(isset($_COOKIE['cn_users'])){
            $usersStored = unserialize(base64_decode($_COOKIE['cn_users']));
        }else{
            $usersStored = [];
        }

        if(isset($usersStored[$user_id])){
            $usersStored[$user_id]['last_connexion'] = new \DateTime();
        }

        uasort($usersStored, function ($a, $b) {
            return strcmp($b['last_connexion']->getTimestamp(), $a['last_connexion']->getTimestamp());
        });

        $response = $this->render('main/auth.html.twig',[
            'usersStored' => $usersStored,
        ]);
        $cookie = new Cookie("cn_users", base64_encode(serialize($usersStored)), time()+(86400*30),null, null, true, true,  false, 'strict' );
        $response->headers->setCookie($cookie);
        $session->remove('userInfo');


        return $response;
    }

    /**
     * @Route("/resetpassword/{hashId}", name="resetpassword")
     * @param $hashId
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function resetpasswordAction($hashId){
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('AppBundle\Entity\Users')
            ->findOneBy(['forgotPwdCode' => $hashId]);

        if($user != null) {
            return $this->render('main/resetpassword.html.twig', [
                'hash_id' => $hashId,
            ]);
        }
        return $this->redirectToRoute('auth');
    }

    /**
     * @Route("/inscription2/{hash_id}", name="inscription2")
     */
    public function inscription2Action($hash_id){

        if(isset(explode('_', base64_decode(base64_decode($hash_id)))[1])){
            $id_user = intval(explode('_', base64_decode(base64_decode($hash_id)))[1]);
        }else{
            $id_user = 0;
        }

        if($id_user != 0){
            $em = $this->getDoctrine()->getManager();
            $user = $em->getRepository('AppBundle\Entity\Users')
                ->find($id_user);

            if($user != null) {

                if($user->getSubscription()[0]->getStatus() == 'VALIDATED'){
                    return $this->redirectToRoute('auth');
                }else{
                    $user->setStatus('VALIDATED');

                    $subscription = $em->getRepository('AppBundle:Subscription')
                        ->findOneBy(['user' => $user]);

                    $date = new \DateTime();
                    $interval = new \DateInterval('P1Y');
                    $date->add($interval);
                    $subscription->setEndContractAt($date);
                    $subscription->setStatus('VALIDATED');

                    $em->persist($user);

                    $stat = new Statistics();
                    $stat->setActionName('INSCRIPTION - VALIDATED');
                    $stat->setUser($user);
                    $em->persist($stat);

                    $em->flush();


                    $session = new Session();
                    if($user){
                        $mem_email = false;
                        $stay_connected = false;


                        if($user->getRole() === 'CLIENT'){
                            $data = [
                                'profile' => $user,
                                'mem_email' => $mem_email,
                                'quizz_id' => $user->getQuizz()[0]->getCustomId(),
                                'quizz001' => $user->getQuizz()[0]->getQuizz001(),
                                'quizz004' => $user->getQuizz()[0]->getQuizz004(),
                                'quizz006' => $user->getQuizz()[0]->getQuizz006(),
                                'quizz008' => $user->getQuizz()[0]->getQuizz008(),
                                'quizz007' => $user->getQuizz()[0]->getQuizz007(),
                                'quizz009' => $user->getQuizz()[0]->getQuizz009(),
                                'quizz011' => $user->getQuizz()[0]->getQuizz011(),
                                'quizz012' => $user->getQuizz()[0]->getQuizz012(),
                                'stay_connected' => $stay_connected
                            ];
                        }else{
                            $data = [
                                'profile' => $user,
                                'mem_email' => $mem_email,
                                'stay_connected' => $stay_connected
                            ];
                        }
                        $session->set('userInfo', $data);
                    }

                    return $this->redirectToRoute('auth');
                }
            }
            return $this->render('main/inscription2.html.twig', [
                'error' => 'Il semble que votre identité n\'a pu être vérifiée. Veuillez prendre contact avec l\'administrateur du site afin d\'en savoir plus.',
            ]);
        }else{
            return $this->render('main/inscription2.html.twig', [
                'error' => 'Il semble que votre identité n\'a pu être vérifiée. Veuillez prendre contact avec l\'administrateur du site afin d\'en savoir plus.',
            ]);
        }
    }

    /**
     * @Route("/administration", name="administration")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function administrationAction(Request $request){

        $session = $request->getSession();

        if(isset($_COOKIE['cn_users'])){
            $usersStored = unserialize(base64_decode($_COOKIE['cn_users']));
        }else{
            $usersStored = [];
        }

        uasort($usersStored, function ($a, $b) {
            return strcmp($b['last_connexion']->getTimestamp(), $a['last_connexion']->getTimestamp());
        });


        if($session->has('userInfo')){
            if($session->get('userInfo')['profile']->getRole() == 'ADMINISTRATOR'){
                return $this->redirectToRoute('dashboard');
            }else{
                return $this->redirectToRoute('main', array('page' => 'accueil'));
            }
        }


        return $this->render('main/administration.html.twig', [
            'usersStored' => $usersStored,
            'recaptcha_key' => $this->getParameter('recaptcha_key')
        ]);

    }

    /**
     * @Route("/home", name="auth")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function authAction(Request $request){

        $session = $request->getSession();

        if(isset($_COOKIE['cn_users'])){
            $usersStored = unserialize(base64_decode($_COOKIE['cn_users']));
        }else{
            $usersStored = [];
        }

        uasort($usersStored, function ($a, $b) {
            return strcmp($b['last_connexion']->getTimestamp(), $a['last_connexion']->getTimestamp());
        });

        if($request->isMethod('POST')){


            $em = $this->getDoctrine()->getManager();

            $mem_email = false;
            $stay_connected = false;

            if(isset($_POST['email']) && isset($_POST['password'])){
                //Auth by mail/password
                if(!isset($_POST['email']) || $_POST['email'] == ''
                    || !isset($_POST['password']) || $_POST['password'] == ''){
                    $error = ['title' => 'Information manquante', 'msg' => 'Veuillez remplir tous les champs'];
                    return $this->render('main/administration.html.twig',[
                        'usersStored' => $usersStored,
                        'error' => $error,
                        'data' => $_POST,
                        'recaptcha_key' => $this->getParameter('recaptcha_key')
                    ]);
                }

                $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$this->getParameter('recaptcha_secret').'&response='.$_POST['g-recaptcha-response']);
                $responseData = json_decode($verifyResponse);
                if(!$responseData->success){
                    $error = ['title' => 'Une erreur est survenue', 'msg' => 'Le captcha est à valider'];
                    return $this->render('main/administration.html.twig',[
                        'usersStored' => $usersStored,
                        'recaptcha_key' => $this->getParameter('recaptcha_key'),
                        'error' => $error,
                        'data' => $_POST
                    ]);
                }

                $user = $em->getRepository('AppBundle:Users')
                    ->findActiveUserByMailAndPassword($_POST['email'], $_POST['password']);


            }else if(!isset($_POST['token']) || $_POST['token'] == ''){
                //Auth by mail2/password2
                if(!isset($_POST['email2']) || $_POST['email2'] == ''
                    || !isset($_POST['password2']) || $_POST['password2'] == ''){
                    $error = ['title' => 'Information manquante', 'msg' => 'Veuillez remplir tous les champs'];
                    return $this->render('main/administration.html.twig',[
                        'usersStored' => $usersStored,
                        'error' => $error,
                        'data' => $_POST,
                        'recaptcha_key' => $this->getParameter('recaptcha_key')
                    ]);
                }
                $user = $em->getRepository('AppBundle:Users')
                    ->findActiveUserByMailAndPassword($_POST['email2'], $_POST['password2']);

                $mem_email = true;
            }else{
                //Auth by token
                if(!isset($_POST['token']) || $_POST['token'] == ''){
                    $error = ['title' => 'Information manquante', 'msg' => 'Veuillez remplir tous les champs'];
                    return $this->render('main/administration.html.twig',[
                        'usersStored' => $usersStored,
                        'error' => $error,
                        'data' => $_POST,
                        'recaptcha_key' => $this->getParameter('recaptcha_key')
                    ]);
                }
                $user = $em->getRepository('AppBundle:Users')
                    ->findActiveUserByToken($_POST['token']);

                $mem_email = true;
                $stay_connected = true;
            }


            if(!$user){
                $error = ['title' => 'Identification incorrect',
                    'msg' => 'Veuillez vérifier vos identifiants'];
                return $this->render('main/administration.html.twig',[
                    'usersStored' => $usersStored,
                    'error' => $error,
                    'data' => $_POST,
                    'recaptcha_key' => $this->getParameter('recaptcha_key')
                ]);
            }

            $subscription = $user->getSubscription()[0];
            if($subscription && $subscription->getStatus() == 'PENDING'){
                $error = ['title' => 'Identification refusée', 'msg' => 'Votre cotisation n\'est pas à jour'];
                return $this->render('main/auth.html.twig',[
                    'usersStored' => $usersStored,
                    'error' => $error,
                    'data' => $_POST,
                    'recaptcha_key' => $this->getParameter('recaptcha_key')
                ]);
            }

            if($mem_email == false){
                $mem_email = (isset($_POST['mem_email']) ? true : false);
            }
            if($stay_connected == false){
                $stay_connected = (isset($_POST['stay_connected']) ? true : false);
            }

            /**
             * @var $user Users
             */
            if($user->getRole() === 'CLIENT'){
                $data = [
                    'profile' => $user,
                    'mem_email' => $mem_email,
                    'quizz_id' => $user->getQuizz()[0]->getCustomId(),
                    'quizz001' => $user->getQuizz()[0]->getQuizz001(),
                    'quizz004' => $user->getQuizz()[0]->getQuizz004(),
                    'quizz006' => $user->getQuizz()[0]->getQuizz006(),
                    'quizz008' => $user->getQuizz()[0]->getQuizz008(),
                    'quizz007' => $user->getQuizz()[0]->getQuizz007(),
                    'quizz009' => $user->getQuizz()[0]->getQuizz009(),
                    'quizz011' => $user->getQuizz()[0]->getQuizz011(),
                    'quizz012' => $user->getQuizz()[0]->getQuizz012(),
                    'stay_connected' => $stay_connected
                ];
            }else{
                $data = [
                    'profile' => $user,
                    'mem_email' => $mem_email,
                    'stay_connected' => $stay_connected
                ];
            }

            $session->set('userInfo', $data);

        }

        else if(!empty($_COOKIE["dhomplus_token"]) && isset($_COOKIE["dhomplus_token"])){
            /* Auth with Cookie dhomplus_token */

            /*get Data Find Active User By Token */
            $em = $this->getDoctrine()->getManager();
            $user = $em->getRepository('AppBundle:Users')
                ->findActiveUserByToken(unserialize(base64_decode($_COOKIE["dhomplus_token"])));
            /**
             * @var $user Users
             */

            if(!empty($user)){
                $mem_email = false;
                $stay_connected = false;
                if($user->getRole() === 'CLIENT'){
                    $data = [
                        'profile' => $user,
                        'mem_email' => $mem_email,
                        'quizz_id' => $user->getQuizz()[0]->getCustomId(),
                        'quizz001' => $user->getQuizz()[0]->getQuizz001(),
                        'quizz004' => $user->getQuizz()[0]->getQuizz004(),
                        'quizz006' => $user->getQuizz()[0]->getQuizz006(),
                        'quizz008' => $user->getQuizz()[0]->getQuizz008(),
                        'quizz007' => $user->getQuizz()[0]->getQuizz007(),
                        'quizz009' => $user->getQuizz()[0]->getQuizz009(),
                        'quizz011' => $user->getQuizz()[0]->getQuizz011(),
                        'quizz012' => $user->getQuizz()[0]->getQuizz012(),
                        'stay_connected' => $stay_connected
                    ];
                    $session->set('userInfo', $data);

                }else{
                    $data = [
                        'profile' => $user,
                        'mem_email' => $mem_email,
                        'stay_connected' => $stay_connected
                    ];
                }
                $session->set('userInfo', $data);
            }
        }

        if($session->has('userInfo')){
            if($session->get('userInfo')['profile']->getRole() == 'ADMINISTRATOR'){
                return $this->redirectToRoute('dashboard');
            }else{
                return $this->redirectToRoute('main', array('page' => 'accueil'));
            }
        }


        return $this->redirectToRoute('macifRedirect');

        return $this->render('main/auth.html.twig', [
            'usersStored' => $usersStored,
            'recaptcha_key' => $this->getParameter('recaptcha_key')
        ]);

    }

    /**
     * @Route("/logout/{action}", name="logout")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function logoutAction(Request $request, $action){

        $session = $request->getSession();

        if(isset($_COOKIE['cn_users'])){
            $usersStored = unserialize(base64_decode($_COOKIE['cn_users']));
        }else{
            $usersStored = [];
        }


        if($action == 'confirmation'){

            if(!$session->has('userInfo')){
                return $this->redirectToRoute('auth');
            }else{
                return $this->render('main/logout.html.twig', [
                    'usersStored' => $usersStored,
                    'profile' => $session->get('userInfo')['profile']
                ]);
            }

        }else if($action == 'changeuser'){

            $session->remove('userInfo');
            return $this->render('main/administration.html.twig', [
                'usersStored' => $usersStored,
                'changeUser' => true,
                'recaptcha_key' => $this->getParameter('recaptcha_key')
            ]);

        }else{
            if(!empty($request->cookies->get("dhomplus_token"))){
                $response = new Response();
                $response->headers->clearCookie('dhomplus_token');
                $response->send();
            }

            $session->remove('userInfo');

            return $this->redirectToRoute('administration');
        }


    }

    /**
     * @Route("/checkout/{hashId}", name="checkout")
     */
    public function checkoutAction($hashId){


//        if(!$hashId || !isset($_POST['stripeToken'])){
//            return $this->redirectToRoute('auth');
//        }

        if(!$hashId){
            return $this->redirectToRoute('auth');
        }


//        $token  = $_POST['stripeToken'];
        if(!isset(explode('_', base64_decode(base64_decode($hashId)))[1])){
            return $this->redirectToRoute('auth');
        }

        $user_id = intval(explode('_', base64_decode(base64_decode($hashId)))[1]);

        if(!is_integer($user_id) || !isset(explode('_', base64_decode(base64_decode($hashId)))[1])){
            return $this->redirectToRoute('auth');
        }


        $em = $this->getDoctrine()->getManager();

        $user = $em->getRepository('AppBundle:Users')
            ->find($user_id);

        $session = new Session();
        if($user){
            $mem_email = false;
            $stay_connected = false;


            if($user->getRole() === 'CLIENT'){
                $data = [
                    'profile' => $user,
                    'mem_email' => $mem_email,
                    'quizz_id' => $user->getQuizz()[0]->getCustomId(),
                    'quizz001' => $user->getQuizz()[0]->getQuizz001(),
                    'quizz004' => $user->getQuizz()[0]->getQuizz004(),
                    'quizz006' => $user->getQuizz()[0]->getQuizz006(),
                    'quizz008' => $user->getQuizz()[0]->getQuizz008(),
                    'quizz007' => $user->getQuizz()[0]->getQuizz007(),
                    'quizz009' => $user->getQuizz()[0]->getQuizz009(),
                    'quizz011' => $user->getQuizz()[0]->getQuizz011(),
                    'quizz012' => $user->getQuizz()[0]->getQuizz012(),
                    'stay_connected' => $stay_connected
                ];
            }else{
                $data = [
                    'profile' => $user,
                    'mem_email' => $mem_email,
                    'stay_connected' => $stay_connected
                ];
            }
            $session->set('userInfo', $data);
        }

        $appInfo = $em->getRepository('AppBundle:Appinfo')
            ->find(1);
/*
        if(strtolower($token) != strtolower($appInfo->getPromoCode())){
            return $this->render('main/inscription2.html.twig', [
                'success' => 'SUCCESS',
                'hash_id' => base64_encode(base64_encode('STKSOLUTIONS_'.$user_id)),
                'publishable_key' => $this->container->getParameter('stripe_publishable_key'),
                'appInfo' => $appInfo,
                'email' => $user->getEmail(),
                'error' => 'ERR_CODE'
            ]);
        }
*/
//        Stripe::setApiKey($this->container->getParameter('stripe_secret_key'));
//
//        $customer = Customer::create(array(
//            'email' => $user->getEmail(),
//            'source'  => $token
//        ));
//
//        $charge = Charge::create(array(
//            'customer' => $customer->id,
//            'amount'   => ($appInfo->getSubscriptionPrice() * 100),
//            'currency' => 'eur'
//        ));

        $subscription = $em->getRepository('AppBundle:Subscription')
            ->findOneBy(['user' => $user]);

        $date = new \DateTime();
        $interval = new \DateInterval('P1Y');
        $date->add($interval);
        $subscription->setEndContractAt($date);
        $subscription->setStatus('VALIDATED');

        $em->flush();

        if(isset($_COOKIE['cn_users'])){
            $usersStored = unserialize(base64_decode($_COOKIE['cn_users']));
        }else{
            $usersStored = [];
        }

        return $this->redirectToRoute('auth');

/*        return $this->render('main/auth.html.twig', [
            'usersStored' => $usersStored,
            'success_message' => 'SUCCESS',
            'data' => ['email' => $user->getEmail()]
        ]);*/
    }

    /**
     * @Route("/dashboard", name="dashboard")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function dashboardAction(Request $request){

        $session = $request->getSession();
        if(!$session->has('userInfo') || $session->get('userInfo')['profile']->getRole() != 'ADMINISTRATOR'){
            return $this->redirectToRoute('auth');
        }

        $em = $this->getDoctrine()->getManager();
        $appInfo = $em->getRepository('AppBundle:Appinfo')->find(1);


        $response = $this->render('main/dashboard.html.twig', [
            'appinfo' => $appInfo,
            'userInfo' => $session->get('userInfo'),
        ]);

        if($session->get('userInfo')['mem_email'] == true){
            if(isset($_COOKIE['cn_users'])){
                $data = unserialize(base64_decode($_COOKIE['cn_users']));
            }else{
                $data = [];
            }
            $data[$session->get('userInfo')['profile']->getId()]['firstname'] = $session->get('userInfo')['profile']->getFirstname();
            $data[$session->get('userInfo')['profile']->getId()]['lastname'] = $session->get('userInfo')['profile']->getLastname();
            $data[$session->get('userInfo')['profile']->getId()]['email'] = $session->get('userInfo')['profile']->getEmail();
            $data[$session->get('userInfo')['profile']->getId()]['token'] = "";
            $data[$session->get('userInfo')['profile']->getId()]['last_connexion'] = new \DateTime();

            $cookie = new Cookie("cn_users", base64_encode(serialize($data)), time()+(86400*30),null, null, true, true,  false, 'strict');
            $response->headers->setCookie($cookie);
        }


        if($session->get('userInfo')['stay_connected'] == true){
            if(isset($_COOKIE['cn_users'])){
                $data = unserialize(base64_decode($_COOKIE['cn_users']));
            }else{
                $data = [];
            }
            $data[$session->get('userInfo')['profile']->getId()]['firstname'] = $session->get('userInfo')['profile']->getFirstname();
            $data[$session->get('userInfo')['profile']->getId()]['lastname'] = $session->get('userInfo')['profile']->getLastname();
            $data[$session->get('userInfo')['profile']->getId()]['email'] = $session->get('userInfo')['profile']->getEmail();
            $data[$session->get('userInfo')['profile']->getId()]['token'] = $session->get('userInfo')['profile']->getToken();
            $data[$session->get('userInfo')['profile']->getId()]['last_connexion'] = new \DateTime();

            $cookie = new Cookie("cn_users", base64_encode(serialize($data)), time()+(86400*30),null, null, true, true,  false, 'strict');
            $response->headers->setCookie($cookie);
        }

        if($session->get('userInfo')['stay_connected'] == false && $session->get('userInfo')['mem_email'] == false){
            if(isset($_COOKIE['cn_users'])){
                $data = unserialize(base64_decode($_COOKIE['cn_users']));
            }else{
                $data = [];
            }
            unset($data[$session->get('userInfo')['profile']->getId()]);
            $cookie = new Cookie("cn_users", base64_encode(serialize($data)), time()+(86400*30),null, null, true, true,  false, 'strict');
            $response->headers->setCookie($cookie);
        }


        return $response;

    }

    /**
     * @Route("/{page}", name="main")
     * @param Request $request
     * @param $page
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function accueilAction(Request $request, $page){

        $session = $request->getSession();
        if(!$session->has('userInfo')){
            return $this->redirectToRoute('auth');
        }else{
            if($session->get('userInfo')['profile']->getRole() == 'ADMINISTRATOR'){
                return $this->redirectToRoute('dashboard');
            }
        }

        $em = $this->getDoctrine()->getManager();
        $appInfo = $em->getRepository('AppBundle:Appinfo')->find(1);

        if($page == null || $page == '' || $page == 'accueil'){
            $page = 'accueil';
            $this->recordAction('PAGE_ACCUEIL', $session->get('userInfo')['profile']->getId());
        }else if($page == 'conseillerpersonnel'){
            $this->recordAction('PAGE_CONSEILLER_PERSONNEL', $session->get('userInfo')['profile']->getId());
        }

//        return $this->render('main/accueil.html.twig', [
//            'appinfo' => $appInfo,
//            'userInfo' => $session->get('userInfo'),
//            'page' => $page
//        ]);

        $response = $this->render('main/accueil.html.twig', [
            'appinfo' => $appInfo,
            'userInfo' => $session->get('userInfo'),
            'page' => $page
        ]);

        if($session->get('userInfo')['mem_email'] == true){
            if(isset($_COOKIE['cn_users'])){
                $data = unserialize(base64_decode($_COOKIE['cn_users']));
            }else{
                $data = [];
            }
            $data[$session->get('userInfo')['profile']->getId()]['firstname'] = $session->get('userInfo')['profile']->getFirstname();
            $data[$session->get('userInfo')['profile']->getId()]['lastname'] = $session->get('userInfo')['profile']->getLastname();
            $data[$session->get('userInfo')['profile']->getId()]['email'] = $session->get('userInfo')['profile']->getEmail();
            $data[$session->get('userInfo')['profile']->getId()]['token'] = "";
            $data[$session->get('userInfo')['profile']->getId()]['last_connexion'] = new \DateTime();

            $cookie = new Cookie("cn_users", base64_encode(serialize($data)), time()+(86400*30),null, null, true, true,  false, 'strict');
            $response->headers->setCookie($cookie);
        }


        if($session->get('userInfo')['stay_connected'] == true){
            if(isset($_COOKIE['cn_users'])){
                $data = unserialize(base64_decode($_COOKIE['cn_users']));
            }else{
                $data = [];
            }
            $data[$session->get('userInfo')['profile']->getId()]['firstname'] = $session->get('userInfo')['profile']->getFirstname();
            $data[$session->get('userInfo')['profile']->getId()]['lastname'] = $session->get('userInfo')['profile']->getLastname();
            $data[$session->get('userInfo')['profile']->getId()]['email'] = $session->get('userInfo')['profile']->getEmail();
            $data[$session->get('userInfo')['profile']->getId()]['token'] = $session->get('userInfo')['profile']->getToken();
            $data[$session->get('userInfo')['profile']->getId()]['last_connexion'] = new \DateTime();

            $cookie = new Cookie("cn_users", base64_encode(serialize($data)), time()+(86400*30),null, null, true, true,  false, 'strict');
            $response->headers->setCookie($cookie);
        }

        if($session->get('userInfo')['stay_connected'] == false && $session->get('userInfo')['mem_email'] == false){
            if(isset($_COOKIE['cn_users'])){
                $data = unserialize(base64_decode($_COOKIE['cn_users']));
            }else{
                $data = [];
            }
            unset($data[$session->get('userInfo')['profile']->getId()]);
            $cookie = new Cookie("cn_users", base64_encode(serialize($data)), time()+(86400*30),null, null, true, true,  false, 'strict');
            $response->headers->setCookie($cookie);
        }


        return $response;

    }

    /**
     * @Route("/sso/{token}", name="sso")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function ssoAction(Request $request, $token)
    {
        if($request->isMethod('GET')) {
            /*get Data Find Active User By Token */
            $em = $this->getDoctrine()->getManager();
            $user = $em->getRepository('AppBundle:Users')
                ->findActiveUserByToken($token);

            if(!empty($user)){
                $response = new Response();
                $cookie = new Cookie("dhomplus_token", serialize($token), time()+(86400*9999));
                $response->headers->setCookie($cookie);
                $response->send();
                return $this->redirectToRoute('auth');
            }
        }
        return $this->redirectToRoute('app_main_insciption');

    }

}
