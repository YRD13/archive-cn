<?php
/**
 * Created by PhpStorm.
 * User: yr
 * Date: 17/11/2017
 * Time: 03:08
 */

namespace AppBundle\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="appinfo")
 */
class Appinfo
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     */
    private $generalCondition;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $subscriptionPrice;

    /**
     * @ORM\Column(type="text")
     */
    private $persoMessageCallConseiller;

    /**
     * @ORM\Column(type="string")
     */
    private $persoConseillerPhone;

    /**
     * @ORM\Column(type="string")
     */
    private $persoConseillerMailDirect;

    /**
     * @ORM\Column(type="string")
     */
    private $persoWebsiteUrl;

    /**
     * @ORM\Column(type="text")
     */
    private $trMessageCallConseiller;

    /**
     * @ORM\Column(type="string")
     */
    private $trConseillerPhone;

    /**
     * @ORM\Column(type="string")
     */
    private $trConseillerMailCallback;

    /**
     * @ORM\Column(type="string")
     */
    private $trConseillerMailDirect;

    /**
     * @ORM\Column(type="string")
     */
    private $trWebsiteAccueil;

    /**
     * @ORM\Column(type="string")
     */
    private $trWebsiteSante;

    /**
     * @ORM\Column(type="string")
     */
    private $trWebsitePrevoyance;

    /**
     * @ORM\Column(type="string")
     */
    private $trWebsiteTeleassistance;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $promoCode;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $perso_conseiller_mail_callback;

    /**
     * @ORM\Column(type="boolean", nullable=false, options={"default": false})
     */
    private $hideRatingAfterMark;



    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getGeneralCondition()
    {
        return $this->generalCondition;
    }

    /**
     * @param mixed $generalCondition
     */
    public function setGeneralCondition($generalCondition)
    {
        $this->generalCondition = $generalCondition;
    }

    /**
     * @return mixed
     */
    public function getPersoMessageCallConseiller()
    {
        return $this->persoMessageCallConseiller;
    }

    /**
     * @param mixed $persoMessageCallConseiller
     */
    public function setPersoMessageCallConseiller($persoMessageCallConseiller)
    {
        $this->persoMessageCallConseiller = $persoMessageCallConseiller;
    }

    /**
     * @return mixed
     */
    public function getPersoConseillerPhone()
    {
        return $this->persoConseillerPhone;
    }

    /**
     * @param mixed $persoConseillerPhone
     */
    public function setPersoConseillerPhone($persoConseillerPhone)
    {
        $this->persoConseillerPhone = $persoConseillerPhone;
    }

    /**
     * @return mixed
     */
    public function getPersoConseillerMailDirect()
    {
        return $this->persoConseillerMailDirect;
    }

    /**
     * @param mixed $persoConseillerMailDirect
     */
    public function setPersoConseillerMailDirect($persoConseillerMailDirect)
    {
        $this->persoConseillerMailDirect = $persoConseillerMailDirect;
    }

    /**
     * @return mixed
     */
    public function getPersoWebsiteUrl()
    {
        return $this->persoWebsiteUrl;
    }

    /**
     * @param mixed $persoWebsiteUrl
     */
    public function setPersoWebsiteUrl($persoWebsiteUrl)
    {
        $this->persoWebsiteUrl = $persoWebsiteUrl;
    }

    /**
     * @return mixed
     */
    public function getTrMessageCallConseiller()
    {
        return $this->trMessageCallConseiller;
    }

    /**
     * @param mixed $trMessageCallConseiller
     */
    public function setTrMessageCallConseiller($trMessageCallConseiller)
    {
        $this->trMessageCallConseiller = $trMessageCallConseiller;
    }

    /**
     * @return mixed
     */
    public function getTrConseillerPhone()
    {
        return $this->trConseillerPhone;
    }

    /**
     * @param mixed $trConseillerPhone
     */
    public function setTrConseillerPhone($trConseillerPhone)
    {
        $this->trConseillerPhone = $trConseillerPhone;
    }

    /**
     * @return mixed
     */
    public function getTrConseillerMailCallback()
    {
        return $this->trConseillerMailCallback;
    }

    /**
     * @param mixed $trConseillerMailCallback
     */
    public function setTrConseillerMailCallback($trConseillerMailCallback)
    {
        $this->trConseillerMailCallback = $trConseillerMailCallback;
    }

    /**
     * @return mixed
     */
    public function getTrConseillerMailDirect()
    {
        return $this->trConseillerMailDirect;
    }

    /**
     * @param mixed $trConseillerMailDirect
     */
    public function setTrConseillerMailDirect($trConseillerMailDirect)
    {
        $this->trConseillerMailDirect = $trConseillerMailDirect;
    }

    /**
     * @return mixed
     */
    public function getTrWebsiteAccueil()
    {
        return $this->trWebsiteAccueil;
    }

    /**
     * @param mixed $trWebsiteAccueil
     */
    public function setTrWebsiteAccueil($trWebsiteAccueil)
    {
        $this->trWebsiteAccueil = $trWebsiteAccueil;
    }

    /**
     * @return mixed
     */
    public function getTrWebsiteSante()
    {
        return $this->trWebsiteSante;
    }

    /**
     * @param mixed $trWebsiteSante
     */
    public function setTrWebsiteSante($trWebsiteSante)
    {
        $this->trWebsiteSante = $trWebsiteSante;
    }

    /**
     * @return mixed
     */
    public function getTrWebsitePrevoyance()
    {
        return $this->trWebsitePrevoyance;
    }

    /**
     * @param mixed $trWebsitePrevoyance
     */
    public function setTrWebsitePrevoyance($trWebsitePrevoyance)
    {
        $this->trWebsitePrevoyance = $trWebsitePrevoyance;
    }

    /**
     * @return mixed
     */
    public function getTrWebsiteTeleassistance()
    {
        return $this->trWebsiteTeleassistance;
    }

    /**
     * @param mixed $trWebsiteTeleassistance
     */
    public function setTrWebsiteTeleassistance($trWebsiteTeleassistance)
    {
        $this->trWebsiteTeleassistance = $trWebsiteTeleassistance;
    }

    /**
     * @return mixed
     */
    public function getSubscriptionPrice()
    {
        return $this->subscriptionPrice;
    }

    /**
     * @param mixed $subscriptionPrice
     */
    public function setSubscriptionPrice($subscriptionPrice)
    {
        $this->subscriptionPrice = $subscriptionPrice;
    }

    /**
     * @return mixed
     */
    public function getPromoCode()
    {
        return $this->promoCode;
    }

    /**
     * @param mixed $promoCode
     */
    public function setPromoCode($promoCode)
    {
        $this->promoCode = $promoCode;
    }

    /**
     * @return mixed
     */
    public function getPersoConseillerMailCallback()
    {
        return $this->perso_conseiller_mail_callback;
    }

    /**
     * @param mixed $perso_conseiller_mail_callback
     */
    public function setPersoConseillerMailCallback($perso_conseiller_mail_callback)
    {
        $this->perso_conseiller_mail_callback = $perso_conseiller_mail_callback;
    }

    /**
     * @return mixed
     */
    public function getHideRatingAfterMark()
    {
        return $this->hideRatingAfterMark;
    }

    /**
     * @param mixed $hideRatingAfterMark
     */
    public function setHideRatingAfterMark($hideRatingAfterMark): void
    {
        $this->hideRatingAfterMark = $hideRatingAfterMark;
    }

}