<?php
/**
 * Created by PhpStorm.
 * User: yr
 * Date: 01/06/2018
 * Time: 15:31
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="categories")
 */
class Categories
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @ORM\Column(type="string")
     */
    private $code_name;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Souscategories", mappedBy="categorie")
     * @ORM\OrderBy({"categorie" = "ASC", "id" = "ASC"})
     */
    private $souscategories;

    /**
     * Themes constructor.
     */
    public function __construct()
    {
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getCodeName()
    {
        return $this->code_name;
    }

    /**
     * @param mixed $code_name
     */
    public function setCodeName($code_name)
    {
        $this->code_name = $code_name;
    }

    /**
     * @return mixed
     */
    public function getSouscategories()
    {
        return $this->souscategories;
    }

    /**
     * @param mixed $souscategories
     */
    public function setSouscategories($souscategories)
    {
        $this->souscategories = $souscategories;
    }



}