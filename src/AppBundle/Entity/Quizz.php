<?php
/**
 * Created by PhpStorm.
 * User: yr
 * Date: 01/06/2018
 * Time: 15:31
 */

namespace AppBundle\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="quizz")
 */
class Quizz
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetimetz")
     */
    private $created_at;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Users", inversedBy="quizz")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $user;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $custom_id;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $quizz001;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $quizz004;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $quizz006;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $quizz008;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $quizz007;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $quizz009;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $quizz011;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $quizz012;

    public function __construct()
    {
        $this->created_at = new \DateTime();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param mixed $created_at
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }


    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getCustomId()
    {
        return $this->custom_id;
    }

    /**
     * @param mixed $custom_id
     */
    public function setCustomId($custom_id)
    {
        $this->custom_id = $custom_id;
    }

    /**
     * @return mixed
     */
    public function getQuizz001()
    {
        return $this->quizz001;
    }

    /**
     * @param mixed $quizz001
     */
    public function setQuizz001($quizz001)
    {
        $this->quizz001 = $quizz001;
    }

    /**
     * @return mixed
     */
    public function getQuizz004()
    {
        return $this->quizz004;
    }

    /**
     * @param mixed $quizz004
     */
    public function setQuizz004($quizz004)
    {
        $this->quizz004 = $quizz004;
    }

    /**
     * @return mixed
     */
    public function getQuizz006()
    {
        return $this->quizz006;
    }

    /**
     * @param mixed $quizz006
     */
    public function setQuizz006($quizz006)
    {
        $this->quizz006 = $quizz006;
    }

    /**
     * @return mixed
     */
    public function getQuizz008()
    {
        return $this->quizz008;
    }

    /**
     * @param mixed $quizz008
     */
    public function setQuizz008($quizz008)
    {
        $this->quizz008 = $quizz008;
    }

    /**
     * @return mixed
     */
    public function getQuizz007()
    {
        return $this->quizz007;
    }

    /**
     * @param mixed $quizz007
     */
    public function setQuizz007($quizz007)
    {
        $this->quizz007 = $quizz007;
    }

    /**
     * @return mixed
     */
    public function getQuizz009()
    {
        return $this->quizz009;
    }

    /**
     * @param mixed $quizz009
     */
    public function setQuizz009($quizz009)
    {
        $this->quizz009 = $quizz009;
    }

    /**
     * @return mixed
     */
    public function getQuizz011()
    {
        return $this->quizz011;
    }

    /**
     * @param mixed $quizz011
     */
    public function setQuizz011($quizz011)
    {
        $this->quizz011 = $quizz011;
    }

    /**
     * @return mixed
     */
    public function getQuizz012()
    {
        return $this->quizz012;
    }

    /**
     * @param mixed $quizz012
     */
    public function setQuizz012($quizz012)
    {
        $this->quizz012 = $quizz012;
    }

}