<?php
/**
 * Created by PhpStorm.
 * User: yr
 * Date: 01/06/2018
 * Time: 15:32
 */

namespace AppBundle\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\StatisticsRepository")
 * @ORM\Table(name="statistics")
 */
class Statistics
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $actionName;

    /**
     * @ORM\Column(type="datetimetz")
     */
    private $triggeredAt;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Users", inversedBy="statistics")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $user;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $content_id;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $content_title;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $content_type;

    /**
     * Statistics constructor.
     */
    public function __construct()
    {
        $this->triggeredAt = new \DateTime();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getActionName()
    {
        return $this->actionName;
    }

    /**
     * @param mixed $actionName
     */
    public function setActionName($actionName)
    {
        $this->actionName = $actionName;
    }

    /**
     * @return mixed
     */
    public function getTriggeredAt()
    {
        return $this->triggeredAt;
    }

    /**
     * @param mixed $triggeredAt
     */
    public function setTriggeredAt($triggeredAt)
    {
        $this->triggeredAt = $triggeredAt;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getContentId()
    {
        return $this->content_id;
    }

    /**
     * @param mixed $content_id
     */
    public function setContentId($content_id)
    {
        $this->content_id = $content_id;
    }

    /**
     * @return mixed
     */
    public function getContentTitle()
    {
        return $this->content_title;
    }

    /**
     * @param mixed $content_title
     */
    public function setContentTitle($content_title)
    {
        $this->content_title = $content_title;
    }

    /**
     * @return mixed
     */
    public function getContentType()
    {
        return $this->content_type;
    }

    /**
     * @param mixed $content_type
     */
    public function setContentType($content_type)
    {
        $this->content_type = $content_type;
    }

}