<?php
/**
 * Created by PhpStorm.
 * User: yr
 * Date: 01/06/2018
 * Time: 15:31
 */

namespace AppBundle\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="themes")
 */
class Themes
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(name="`order`", type="integer")
     */
    private $order;

    /**
     * @ORM\Column(type="string")
     */
    private $type;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $url;

    /**
     * @ORM\Column(type="string")
     */
    private $title;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $subtitle;

    /**
     * @ORM\Column(type="string")
     */
    private $category;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $souscategory;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $picture;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $header;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Sousthemes", mappedBy="theme")
     * @ORM\OrderBy({"theme" = "ASC", "id" = "ASC"})
     */
    private $sousthemes;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Souscategories", inversedBy="themes")
     */
    private $souscategorie;


    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $visible;


    /**
     * @ORM\Column(type="datetimetz", nullable=true)
     */
    private $expiration_date;

    /**
     * Themes constructor.
     */
    public function __construct()
    {
        $this->sousthemes = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getSubtitle()
    {
        return $this->subtitle;
    }

    /**
     * @param mixed $subtitle
     */
    public function setSubtitle($subtitle)
    {
        $this->subtitle = $subtitle;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param mixed $category
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }

    /**
     * @return mixed
     */
    public function getPicture()
    {
        return $this->picture;
    }

    /**
     * @param mixed $picture
     */
    public function setPicture($picture)
    {
        $this->picture = $picture;
    }

    /**
     * @return mixed
     */
    public function getHeader()
    {
        return $this->header;
    }

    /**
     * @param mixed $header
     */
    public function setHeader($header)
    {
        $this->header = $header;
    }

    /**
     * @return mixed
     */
    public function getSoustheme()
    {
        return $this->sousthemes;
    }

    /**
     * @param mixed $sousthemes
     */
    public function setSoustheme($sousthemes)
    {
        $this->sousthemes = $sousthemes;
    }

    /**
     * @return mixed
     */
    public function getSouscategory()
    {
        return $this->souscategory;
    }

    /**
     * @param mixed $souscategory
     */
    public function setSouscategory($souscategory)
    {
        $this->souscategory = $souscategory;
    }

    /**
     * @return mixed
     */
    public function getSousthemes()
    {
        return $this->sousthemes;
    }

    /**
     * @param mixed $sousthemes
     */
    public function setSousthemes($sousthemes)
    {
        $this->sousthemes = $sousthemes;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param mixed $order
     */
    public function setOrder($order)
    {
        $this->order = $order;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return mixed
     */
    public function getVisible()
    {
        return $this->visible;
    }

    /**
     * @param mixed $visible
     */
    public function setVisible($visible)
    {
        $this->visible = $visible;
    }

    /**
     * @return mixed
     */
    public function getExpirationDate()
    {
        return $this->expiration_date;
    }

    /**
     * @param mixed $expiration_date
     */
    public function setExpirationDate($expiration_date)
    {
        $this->expiration_date = $expiration_date;
    }

    /**
     * @return mixed
     */
    public function getSouscategorie()
    {
        return $this->souscategorie;
    }

    /**
     * @param mixed $souscategorie
     */
    public function setSouscategorie($souscategorie)
    {
        $this->souscategorie = $souscategorie;
    }

}