<?php
/**
 * Created by PhpStorm.
 * User: yr
 * Date: 07/12/2017
 * Time: 23:40
 */

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class StatisticsRepository extends EntityRepository
{
    /**
     * @return mixed
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getExportStats(){

        $rawSql = "SELECT
            action_name,
            to_char(triggered_at at time zone 'Europe/Paris', 'DD/MM/YYYY HH24:MI:SS') AS triggered_at,
            users.firstname,
            users.lastname,
            users.email,
            users.phone,
            users.birth_date, users.member_num, 
            quizz.custom_id AS id_quizz_macif,
            content_id,
            content_title,
            content_type,
            to_char(NOW() - INTERVAL '1 DAY', 'YYYY-MM-DD HH24:MI:SS') AS test
            FROM statistics 
            LEFT JOIN users ON users.id = statistics.user_id
            LEFT JOIN quizz ON quizz.user_id = users.id
            /*WHERE to_char(triggered_at at time zone 'Europe/Paris', 'YYYY-MM-DD') > to_char(NOW() - INTERVAL '2 DAY', 'YYYY-MM-DD')*/
            ORDER BY statistics.id ASC";

        $stmt = $this->getEntityManager()->getConnection()->prepare($rawSql);
        $stmt->execute([]);

        return $stmt->fetchAll();
    }
}