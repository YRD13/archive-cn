<?php
/**
 * Created by PhpStorm.
 * User: yr
 * Date: 07/12/2017
 * Time: 23:40
 */

namespace AppBundle\Repository;


use Doctrine\ORM\EntityRepository;

class UsersRepository extends EntityRepository
{
    /**
     * @param $email
     * @param $password
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findActiveUserByMailAndPassword($email, $password){

        return $this->createQueryBuilder('users')
            ->leftJoin('users.subscription', 'subscription')
            ->leftJoin('users.quizz', 'quizz')
            ->andWhere('users.email = :email')
            ->andWhere('users.password = :password')
            ->andWhere("users.status = 'VALIDATED'")
            ->setParameter('email', $email)
            ->setParameter('password', md5($password))
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @param $token
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findActiveUserByToken($token){

        return $this->createQueryBuilder('users')
            ->leftJoin('users.subscription', 'subscription')
            ->leftJoin('users.quizz', 'quizz')
            ->andWhere('users.token = :token')
            ->andWhere("users.status = 'VALIDATED'")
            ->setParameter('token', $token)
            ->getQuery()
            ->getOneOrNullResult();
    }


    /**
     * @return mixed
     */
    public function getClients(){

        return $this->createQueryBuilder('users')
            ->leftJoin('users.subscription', 'subscription')
            ->andWhere("users.role != 'ADMINISTRATOR'")
            ->getQuery()
            ->execute();
    }


    /**
     * Unused function
     * @return mixed
     * @throws \Doctrine\DBAL\DBALException

    public function getExportClients(){

        $rawSql = "SELECT 
          users.id AS id_cn,
          users.lastname AS nom,
          users.firstname AS prenom,
          users.email AS adresse_mail,
          users.phone AS telephone,
          to_char(users.created_at, 'DD/MM/YYYY') AS date_souscription_service,
          (SELECT count(*) FROM statistics WHERE user_id = users.id AND action_name = 'APPEL_CS') AS nb_appel,
          (SELECT count(*) FROM statistics WHERE user_id = users.id AND action_name = 'MAIL_CS') AS nb_email,
          (SELECT count(*) FROM statistics WHERE user_id = users.id AND action_name = 'RAPPEL_CS') AS nb_demande,
          (SELECT custom_id from quizz WHERE quizz.user_id = users.id LIMIT 1) AS id_quizz_macif
          FROM users 
          LEFT JOIN subscription ON subscription.user_id = users.id
          ORDER BY users.id ASC";

        $stmt = $this->getEntityManager()->getConnection()->prepare($rawSql);
        $stmt->execute([]);

        return $stmt->fetchAll();
    }*/
}