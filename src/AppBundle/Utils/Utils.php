<?php
/**
 * Created by PhpStorm.
 * User: yr
 * Date: 14/05/2020
 * Time: 23:30
 */

namespace AppBundle\Utils;


use Firebase\JWT\JWT;

class Utils
{
    /**
     * Generate query string for openId call
     *
     * @param $params
     * @return string
     */
    public function createQueryString($params){

        $query_array = [];

        foreach( $params as $key => $key_value ){
            $query_array[] = $key . '=' . $key_value;
        }

        $queryString = implode( '&', $query_array );

        return $queryString;

    }

    /**
     * Generate one day hash for openId purpose
     *
     * @param string $secret
     * @return string
     */
    public function generateOneDayHash($secret){

        return hash_hmac(
            'sha256',
            'stream-techs' . date('Y-m-d'),
            $secret
        );
    }

    /**
     * Generate state for openId call
     *
     * @param string $nonce
     * @param string $secret
     * @return string
     */
    public function generateState($nonce, $secret){

        $payload = [
            "hash" => $this->generateOneDayHash($nonce)
        ];


        return JWT::encode($payload, $secret);

    }

    /**
     * Check if state is valid
     *
     * @param string $nonce
     * @param string $secret
     * @param string $receivedState
     * @return bool
     */
    public function checkStateIsValid($nonce, $secret, $receivedState){

        try{
            $decoded = (array) JWT::decode($receivedState, $secret, array('HS256'));
        }catch(\Exception $exception){
            return false;
        }

        if(!array_key_exists('hash', $decoded))
            return false;

        return ($this->generateOneDayHash($nonce) == $decoded['hash']);

    }

    /**
     * Check if nonce is valid
     *
     * @param string $secret
     * @param string $receivedNonce
     * @return bool
     */
    public function checkNonceIsValid($secret, $receivedNonce){

        return ($this->generateOneDayHash($secret) == $receivedNonce);

    }

    /**
     * Decode a JWT without verifying signature
     *
     * @param $jwt
     * @return bool|string
     */
    public function jwtDecodeWithoutKey($jwt){
        list($header, $payload, $signature) = explode (".", $jwt);

        try{
            return json_decode(base64_decode($payload), true);
        }catch(\Exception $e){
            return false;
        }
    }
}